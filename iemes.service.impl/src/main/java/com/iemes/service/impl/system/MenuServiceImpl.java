package com.iemes.service.impl.system;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.ResFormMap;
import com.iemes.entity.RoleFormMap;
import com.iemes.entity.RoleResFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.ResourcesMapper;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.system.MenuService;
import com.iemes.util.ListUtils;
import com.iemes.util.UUIDUtils;

@Service
public class MenuServiceImpl implements MenuService {
	
	@Inject
	private ResourcesMapper resourcesMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveMenu(ResFormMap resFormMap) throws BusinessException {
		try {
			String id = resFormMap.getStr("id");
			String res_key = resFormMap.getStr("res_key");
			
			if(StringUtils.isEmpty(res_key)) {
				throw new BusinessException("菜单编号不能为空，请输入！");
			}
			
			ResFormMap resFormMap2 = new ResFormMap();
			resFormMap2.put("res_key", res_key);
			List<ResFormMap> listResFormMap = resourcesMapper.findByNames(resFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(listResFormMap)) {
				//更新 
				ResFormMap resFormMap3 = new ResFormMap();
				resFormMap3 = listResFormMap.get(0);
				id = resFormMap3.getStr("id");
				resFormMap.set("id", id);
				
				resourcesMapper.editEntity(resFormMap);
			}else {
				//新增
				resFormMap.put("id", UUIDUtils.getUUID());
				resourcesMapper.addEntity(resFormMap);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("菜单保存失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delMenu(String id) throws BusinessException {
		try {
			//1.如果该及节点下有子节点，则要求先清空该节点下的子节点后，才可以删除该节点		
			ResFormMap resFormMap2 = new ResFormMap();
			resFormMap2.put("parent_id", id);
			resFormMap2.put("orderby", " order by level");
			List<ResFormMap> listChildResFormMap = resourcesMapper.findByNames(resFormMap2);
			if(ListUtils.isNotNull(listChildResFormMap)) {	
				String chlidResName = "";
				for(int i=0;i<listChildResFormMap.size();i++) {
					ResFormMap map = listChildResFormMap.get(i);
					if(StringUtils.isEmpty(chlidResName)) {
						chlidResName = map.getStr("res_name");
					}else {
						chlidResName += "," + map.getStr("res_name");
					}
				}
				throw new BusinessException("此菜单含有子节点 " + chlidResName + "，无法执行删除！");
			}
			
			//2.删除菜单本身的信息
			//resourcesMapper.deleteByAttribute("id", id, ResourcesMapper.class);
			ResFormMap resFormMap = new ResFormMap();
			resFormMap.put("id", id);
			resourcesMapper.deleteByNames(resFormMap);
			
			//3.如果菜单被分配到了角色，还需要删除角色的菜单信息
			RoleResFormMap roleResFormMap = new RoleResFormMap();
			roleResFormMap.put("resId", id);
			resourcesMapper.deleteByNames(roleResFormMap);	
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("菜单删除失败！" + e.getMessage());
		}
	}

}
