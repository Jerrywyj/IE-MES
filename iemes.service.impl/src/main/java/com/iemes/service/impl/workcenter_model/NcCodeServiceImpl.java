package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.NcCodeFormMap;
import com.iemes.entity.NcCodeGroupFormMap;
import com.iemes.entity.NcCodeGroupRelationFormMap;
import com.iemes.entity.SfcNcFormMap;
import com.iemes.entity.UDefinedDataValueFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.NcCodeService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class NcCodeServiceImpl implements NcCodeService {
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private CommonService commonService;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveNcCode(NcCodeFormMap ncCodeFormMap) throws BusinessException {
		try {			
			String id = ncCodeFormMap.getStr("id");
			String ncCode = ncCodeFormMap.getStr("nc_code");
			
			if(StringUtils.isEmpty(ncCode)) {
				throw new BusinessException("不良代码编号不能为空，请输入！");
			}
			
			NcCodeFormMap ncCodeFormMap2 = new NcCodeFormMap();
			ncCodeFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			ncCodeFormMap2.put("nc_code", ncCode);
			List<NcCodeFormMap> listNcCodeFormMap = baseMapper.findByNames(ncCodeFormMap2);
			
			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(listNcCodeFormMap)) {
				//更新 
				NcCodeFormMap ncCodeFormMap3 = new NcCodeFormMap();
				ncCodeFormMap3 = listNcCodeFormMap.get(0);
				id = ncCodeFormMap3.getStr("id");
				ncCodeFormMap.set("id", id);
				
				baseMapper.editEntity(ncCodeFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				ncCodeFormMap.set("id", id);
				baseMapper.addEntity(ncCodeFormMap);
			}

			// 2.添加或更新不良代码的不良代码组信息
			NcCodeGroupRelationFormMap ncCodeGroupRelationFormMap = new NcCodeGroupRelationFormMap();
			ncCodeGroupRelationFormMap.put("nc_code_id", id);
			baseMapper.deleteByNames(ncCodeGroupRelationFormMap);
			String ncCodeGroupId = ncCodeFormMap.getStr("nc_code_group_id");
			if (!StringUtils.isEmpty(ncCodeGroupId)) {
				ncCodeGroupRelationFormMap.put("id", UUIDUtils.getUUID());
				ncCodeGroupRelationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());

				ncCodeGroupRelationFormMap.put("nc_code_group_id", ncCodeGroupId);
				ncCodeGroupRelationFormMap.put("nc_code_id", id);
				ncCodeGroupRelationFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
				ncCodeGroupRelationFormMap.put("create_time", DateUtils.getStringDateTime());

				baseMapper.addEntity(ncCodeGroupRelationFormMap);
			}
			// 3.添加或更新自定义数据信息
			String dataValue = ncCodeFormMap.getStr("udefined_data");
			commonService.saveUDefinedDataValueS(id, dataValue);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("不良代码保存失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delNcCode(String ncCodeId) throws BusinessException {
		try {
			// 1.判断不良代码已被使用，则不允许删除
			SfcNcFormMap sfcNcFormMap = new SfcNcFormMap();
			sfcNcFormMap.put("nc_code_id", ncCodeId);
			List<SfcNcFormMap> listSfcNcFormMap = baseMapper.findByNames(sfcNcFormMap);
			if (ListUtils.isNotNull(listSfcNcFormMap)) {
				throw new BusinessException("此不良代码已用于生产，不可删除！");
			}

			// 2.删除不良代码表
			NcCodeFormMap ncCodeFormMap = new NcCodeFormMap();
			ncCodeFormMap.put("id", ncCodeId);
			baseMapper.deleteByNames(ncCodeFormMap);
			
			//3.删除不良代码和不良代码组的关系				
			NcCodeGroupRelationFormMap ncCodeGroupRelationFormMap = new NcCodeGroupRelationFormMap();
			ncCodeGroupRelationFormMap.put("nc_code_id", ncCodeId);
			baseMapper.deleteByNames(ncCodeGroupRelationFormMap);
			
			//4.删除自定义数据表
			 UDefinedDataValueFormMap uDefinedDataValueFormMap = new UDefinedDataValueFormMap();
			 uDefinedDataValueFormMap.put("data_type_detail_id",ncCodeId);
			 baseMapper.deleteByNames(uDefinedDataValueFormMap);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("不良代码删除失败！" + e.getMessage());
		}
	}

}
