package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.NcCodeFormMap;
import com.iemes.entity.NcCodeGroupFormMap;
import com.iemes.entity.NcCodeGroupRelationFormMap;
import com.iemes.entity.SfcNcFormMap;
import com.iemes.entity.UserFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.CommonService;
import com.iemes.service.workcenter_model.NcCodeGroupService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

@Service
public class NcCodeGroupServiceImpl implements NcCodeGroupService {
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private CommonService commonService;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveNcCodeGroup(NcCodeGroupFormMap ncCodeGroupFormMap) throws BusinessException {
		try {			
			String id = ncCodeGroupFormMap.getStr("id");
			String ncCodeGroupNo = ncCodeGroupFormMap.getStr("nc_code_group_no");
			
			if(StringUtils.isEmpty(ncCodeGroupNo)) {
				throw new BusinessException("不良代码组编号不能为空，请输入！");
			}
			
			NcCodeGroupFormMap ncCodeGroupFormMap2 = new NcCodeGroupFormMap();
			ncCodeGroupFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			ncCodeGroupFormMap2.put("nc_code_group_no", ncCodeGroupNo);
			List<NcCodeGroupFormMap> listNcCodeGroupFormMap = baseMapper.findByNames(ncCodeGroupFormMap2);

			//如果编号存在，就执行更新，不存在就执行新增
			if (ListUtils.isNotNull(listNcCodeGroupFormMap)) {
				//更新 
				NcCodeGroupFormMap ncCodeGroupFormMap3 = new NcCodeGroupFormMap();
				ncCodeGroupFormMap3 = listNcCodeGroupFormMap.get(0);
				id = ncCodeGroupFormMap3.getStr("id");
				ncCodeGroupFormMap.set("id", id);
				
				baseMapper.editEntity(ncCodeGroupFormMap);
			}else {
				//新增
				id = UUIDUtils.getUUID();
				ncCodeGroupFormMap.set("id", id);
				baseMapper.addEntity(ncCodeGroupFormMap);
			}

			// 2.添加或更新不良代码组的不良代码信息
			NcCodeGroupRelationFormMap ncCodeGroupRelationFormMap = new NcCodeGroupRelationFormMap();
			ncCodeGroupRelationFormMap.put("nc_code_group_id", id);
			baseMapper.deleteByNames(ncCodeGroupRelationFormMap);
			String selectedNcCodeList = ncCodeGroupFormMap.getStr("selectedNcCodeList");
			//String unSelectedNcCodeList = ncCodeGroupFormMap.getStr("unSelectedNcCodeList");
			if (!Common.isEmpty(selectedNcCodeList)) {
				String[] ncCodeListArray = selectedNcCodeList.split(",");
				for (String ncCode_id : ncCodeListArray) {
					ncCodeGroupRelationFormMap.put("id", UUIDUtils.getUUID());
					ncCodeGroupRelationFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
					ncCodeGroupRelationFormMap.put("nc_code_group_id", id);
					ncCodeGroupRelationFormMap.put("nc_code_id", ncCode_id);
					ncCodeGroupRelationFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					ncCodeGroupRelationFormMap.put("create_time", DateUtils.getStringDateTime());

					baseMapper.addEntity(ncCodeGroupRelationFormMap);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("不良代码组保存失败！" + e.getMessage());
		}
	}

	@Override
	@Transactional
	public void delNcCodeGroup(String ncCodeGroupId) throws BusinessException {
		try {
			// 1.判断不良代码组已被使用，则不允许删除
			SfcNcFormMap sfcNcFormMap = new SfcNcFormMap();
			sfcNcFormMap.put("nc_code_group_id", ncCodeGroupId);
			List<SfcNcFormMap> listSfcNcFormMap = baseMapper.findByNames(sfcNcFormMap);
			if (ListUtils.isNotNull(listSfcNcFormMap)) {
				throw new BusinessException("此不良代码组已用于生产，不可删除！");
			}

			// 2.删除不良代码组表
			NcCodeGroupFormMap ncCodeGroupFormMap = new NcCodeGroupFormMap();
			ncCodeGroupFormMap.put("id", ncCodeGroupId);
			baseMapper.deleteByNames(ncCodeGroupFormMap);
			
			//3.删除不良代码和不良代码组的关系				
			NcCodeGroupRelationFormMap ncCodeGroupRelationFormMap = new NcCodeGroupRelationFormMap();
			ncCodeGroupRelationFormMap.put("nc_code_group_id", ncCodeGroupId);
			baseMapper.deleteByNames(ncCodeGroupRelationFormMap);
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("不良代码删除失败！" + e.getMessage());
		}
	}

}
