package com.iemes.service.impl.workcenter_model;

import java.util.List;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.iemes.entity.UDefinedDataFieldFormMap;
import com.iemes.entity.UDefinedDataFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.BaseExtMapper;
import com.iemes.service.workcenter_model.UserDefinedDataService;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.UUIDUtils;

import net.sf.json.JSONArray;

@Service
public class UserDefinedDataServiceImpl implements UserDefinedDataService {
	
	@Inject
	private BaseExtMapper baseMapper;
	
	private Logger log = Logger.getLogger(this.getClass());

	@Override
	@Transactional
	public void saveWorkCenter(UDefinedDataFormMap uDefinedDataFormMap) throws BusinessException {
		try {
			
			UDefinedDataFormMap uDefinedDataFormMap2 = new UDefinedDataFormMap();
			uDefinedDataFormMap2.put("data_type", uDefinedDataFormMap.getStr("data_type"));
			uDefinedDataFormMap2.put("site_id", ShiroSecurityHelper.getSiteId());
			List<UDefinedDataFormMap> list = baseMapper.findByNames(uDefinedDataFormMap2);
			
			if (ListUtils.isNotNull(list)) {
				uDefinedDataFormMap.put("id", list.get(0).getStr("id"));
				baseMapper.editEntity(uDefinedDataFormMap);
			}else {
				//插入自定义数据主表
				uDefinedDataFormMap.put("id", UUIDUtils.getUUID());
				baseMapper.addEntity(uDefinedDataFormMap);
			}
			
			//插入自定义数据字段表
			baseMapper.deleteByAttribute("udefined_data_id", uDefinedDataFormMap.getStr("id"), UDefinedDataFieldFormMap.class);
			String dataField = uDefinedDataFormMap.getStr("udefined_data");
			if (!StringUtils.isEmpty(dataField)) {
				JSONArray jsonArray = JSONArray.fromObject(dataField);
				for (int i=0;i<jsonArray.size();i++) {
					JSONArray arr = (JSONArray) jsonArray.get(i);
					UDefinedDataFieldFormMap uDefinedDataFieldFormMap = new UDefinedDataFieldFormMap();
					uDefinedDataFieldFormMap.put("id", UUIDUtils.getUUID());
					uDefinedDataFieldFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
					uDefinedDataFieldFormMap.put("udefined_data_id", uDefinedDataFormMap.getStr("id"));
					uDefinedDataFieldFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
					uDefinedDataFieldFormMap.put("create_time", DateUtils.getStringDateTime());
					uDefinedDataFieldFormMap.put("data_level", arr.getString(0));
					uDefinedDataFieldFormMap.put("data_key", arr.getString(1));
					uDefinedDataFieldFormMap.put("data_label", arr.getString(2));
					baseMapper.addEntity(uDefinedDataFieldFormMap);
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			throw new BusinessException("自定义数据保存失败！"+e.getMessage());
		}
	}

	@Override
	public void delWorkCenter(String uDefinedDataId) throws BusinessException {
		// TODO Auto-generated method stub

	}

}
