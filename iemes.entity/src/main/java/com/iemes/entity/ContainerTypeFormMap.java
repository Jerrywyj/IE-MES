package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_container_type", id="id")
public class ContainerTypeFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3758359975867129684L;

}
