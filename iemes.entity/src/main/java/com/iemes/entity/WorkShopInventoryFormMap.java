package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_workshop_inventory", id="id")
public class WorkShopInventoryFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
