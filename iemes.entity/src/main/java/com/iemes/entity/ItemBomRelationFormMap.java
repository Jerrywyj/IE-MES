package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_item_bom_relation", id="id")
public class ItemBomRelationFormMap extends FormMap<String,Object>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
