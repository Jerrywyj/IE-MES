package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "mds_panel_button", id="id")
public class PodPanelButtonFormMap extends FormMap<String, Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1596881041678807057L;
	
}
