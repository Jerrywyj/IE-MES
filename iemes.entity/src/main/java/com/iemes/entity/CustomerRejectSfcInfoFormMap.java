package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

/**
 * 客退单SFC关联信息
 * @author Administrator
 *
 */
@TableSeg(tableName = "mds_customer_reject_sfcinfo", id="id")
public class CustomerRejectSfcInfoFormMap extends FormMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8758690030413278107L;

	
}
