package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * mds_pod_function实体表
 */
@TableSeg(tableName = "mds_pod_function", id="id")
public class PodFunctionFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author MDS
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
