package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;

@TableSeg(tableName = "ly_grade_calculate", id="id")
public class GradeCalculateFormMap extends FormMap<String,Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4279340958757428045L;

}
