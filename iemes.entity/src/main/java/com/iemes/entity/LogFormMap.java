package com.iemes.entity;

import com.iemes.entity.annotation.TableSeg;
import com.iemes.entity.FormMap;



/**
 * 实体表
 */
@TableSeg(tableName = "mds_log", id="id")
public class LogFormMap extends FormMap<String,Object>{

	/**
	 *@descript
	 *@author MDS
	 *@version 2.0
	 */
	private static final long serialVersionUID = 1L;

}
