package com.iemes.service.builider;

import com.iemes.entity.PageSaveFormMap;
import com.iemes.exception.BusinessException;

public interface BuliderService {
	/**
	 * 保存页面（新增或更新）
	 * @param pageSaveFormMap
	 * @throws BusinessException
	 */
	void savePage(PageSaveFormMap pageSaveFormMap)throws BusinessException;
}
