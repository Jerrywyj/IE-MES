package com.iemes.service;

import com.iemes.entity.ContainerFormMap;
import com.iemes.entity.ContainerSFCFormMap;
import com.iemes.entity.ContainerTypeFormMap;
import com.iemes.exception.BusinessException;

public interface PackingService {

	/**
	 * 保存容器装SFC信息
	 * @param itemBomFormMap
	 * @throws BusinessException
	 */
	void saveContainerSFC(ContainerTypeFormMap containerFormMap)throws BusinessException;

	/**
	 * 包装
	 * @param containerSFCFormMap
	 */
	String packing(ContainerSFCFormMap containerSFCFormMap)throws BusinessException;
	
	/**
	 * 移除
	 * @param containerSFCFormMap
	 * @throws BusinessException
	 */
	void unPacking(ContainerSFCFormMap containerSFCFormMap)throws BusinessException;
	
	/**
	 * 解包
	 * @param containerSFCFormMap
	 * @throws BusinessException
	 */
	void openContainer(ContainerFormMap containerFormMap)throws BusinessException;
	
	/**
	 * 打包
	 * @param containerSFCFormMap
	 * @throws BusinessException
	 */
	void closeContainer(ContainerFormMap containerFormMap)throws BusinessException;
}
