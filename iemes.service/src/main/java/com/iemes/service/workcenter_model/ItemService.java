package com.iemes.service.workcenter_model;

import com.iemes.entity.ItemFormMap;
import com.iemes.exception.BusinessException;

public interface ItemService {

	/**
	 * 保存物料
	 * @param itemFormMap
	 * @throws BusinessException
	 */
	void saveItem(ItemFormMap itemFormMap)throws BusinessException;

	/**
	 * 删除物料
	 * @param itemId
	 * @throws BusinessException
	 */
	void delItem(String itemId)throws BusinessException;
}
