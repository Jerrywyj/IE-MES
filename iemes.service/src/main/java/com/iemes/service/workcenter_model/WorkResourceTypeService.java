package com.iemes.service.workcenter_model;

import com.iemes.entity.WorkResourceTypeFormMap;
import com.iemes.exception.BusinessException;

public interface WorkResourceTypeService {
	
	/**
	 * 保存资源
	 * @param workResourceTypeFormMap
	 * @throws BusinessException
	 */
	void saveWorkResourceType(WorkResourceTypeFormMap workResourceTypeFormMap)throws BusinessException;

	/**
	 * 删除资源
	 * @param workResourceTypeId
	 * @throws BusinessException
	 */
	void delWorkResourceType(String workResourceTypeId)throws BusinessException;
}
