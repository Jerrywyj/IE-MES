package com.iemes.service.WorkScheduling;

import com.iemes.entity.SopFormMap;
import com.iemes.exception.BusinessException;

public interface SopManagerService {
	
	/**
	 * 保存sop
	 * @param sopFormMap
	 * @throws BusinessException
	 */
	void saveSop(SopFormMap sopFormMap)throws BusinessException;
}
