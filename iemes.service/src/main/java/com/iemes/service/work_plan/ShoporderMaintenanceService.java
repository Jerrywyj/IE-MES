package com.iemes.service.work_plan;

import com.iemes.entity.ShoporderFormMap;
import com.iemes.exception.BusinessException;

public interface ShoporderMaintenanceService {
	
	/**
	 * 保存工单
	 * @param shoporderFormMap
	 * @param shoporderStattusBeforeHangup
	 * @throws BusinessException
	 */
	void saveShoporder(ShoporderFormMap shoporderFormMap,int shoporderStattusBeforeHangup)throws BusinessException;

	/**
	 * 删除工单
	 * @param shoporderId
	 * @throws BusinessException
	 */
	void delShoporder(String shoporderId)throws BusinessException;
}
