package com.iemes.service.work_process;

import com.iemes.entity.PodFunctionFormMap;
import com.iemes.exception.BusinessException;

public interface PodFunctionService {

	/**
	 * 保存POD功能
	 * @param podFunctionFormMap
	 * @throws BusinessException
	 */
	void savePodFunction(PodFunctionFormMap podFunctionFormMap)throws BusinessException;

	/**
	 * 删除POD功能
	 * @param podFunctionId
	 * @throws BusinessException
	 */
	void delPodFunction(String podFunctionId)throws BusinessException;
}
