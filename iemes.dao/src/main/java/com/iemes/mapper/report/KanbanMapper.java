package com.iemes.mapper.report;

import java.util.List;
import java.util.Map;

import com.iemes.mapper.base.BaseMapper;
import com.iemes.entity.FormMap;

public interface KanbanMapper extends BaseMapper{
	
	/**
	 * 查询物料消耗信息
	 * @param map
	 * @return
	 */
	public List<Map<String, Object>> findKanBanData(FormMap<String, String> formMap);
}
