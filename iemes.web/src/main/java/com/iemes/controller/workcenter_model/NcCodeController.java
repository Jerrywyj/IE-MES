package com.iemes.controller.workcenter_model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.NcCodeFormMap;
import com.iemes.entity.NcCodeGroupFormMap;
import com.iemes.entity.NcCodeGroupRelationFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.base.BaseMapper;
import com.iemes.service.workcenter_model.NcCodeService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;


/**
 * 
 */
@Controller
@RequestMapping("/workcenter_model/nc_code/")
public class NcCodeController extends BaseController {
	
	@Inject
	private BaseMapper baseMapper;
	
	@Inject
	private NcCodeService ncCodeService;
	
	private Logger log = Logger.getLogger(this.getClass());
	private String ncCodeMaintenanceUrl = Common.BACKGROUND_PATH + "/workcenter_model/nc_code/nc_code_maintenance";
	private String uDefinedDataType = "nc_code_maintenance";

	//页面跳转
	@RequestMapping("nc_code_maintenance")
	public String Get_nc_code_maintenance_Url(Model model,HttpServletRequest request) throws Exception {
		model.addAttribute("res", findByRes());
		model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
		handlePageRes(model,request);
		return ncCodeMaintenanceUrl;
	}
	
	/**
	 * 保存不良代码
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveNcCode")
	@SystemLog(module="不良代码维护",methods="不良代码维护-保存不良代码")		//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveNcCode() {
		NcCodeFormMap ncCodeFormMap = getFormMap(NcCodeFormMap.class);
		ncCodeFormMap.put("create_time", DateUtils.getStringDateTime());
		ncCodeFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		ncCodeFormMap.put("site_id", ShiroSecurityHelper.getSiteId());	
		try {
			ncCodeService.saveNcCode(ncCodeFormMap);	
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	

	/**
	 * 检索不良代码
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryNcCode")
	public String queryNcCode(Model model, HttpServletRequest request) {
		String siteId = ShiroSecurityHelper.getSiteId();
		String ncCode = request.getParameter("nc_code");
		//1.查询不良代码的基本信息
		NcCodeFormMap ncCodeFormMap = new NcCodeFormMap();
		ncCodeFormMap.put("nc_code", ncCode);
		ncCodeFormMap.put("site_id", siteId);
		List<NcCodeFormMap> listNcCodeFormMap= baseMapper.findByNames(ncCodeFormMap);
		if (!ListUtils.isNotNull(listNcCodeFormMap)) {
			String errMessage = "未检索到不良代码为：" + ncCode.toUpperCase() + "的信息";
			log.error(errMessage);
			model.addAttribute("res", findByRes());
			model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return ncCodeMaintenanceUrl;
		}
		
		ncCodeFormMap = listNcCodeFormMap.get(0);
		model.addAttribute("ncCodeFormMap", ncCodeFormMap);
		
		String ncCodeId = ncCodeFormMap.getStr("id");
		//2.查询不良代码的不良代码组信息
		NcCodeGroupRelationFormMap ncCodeGroupRelationFormMap = new NcCodeGroupRelationFormMap();
		ncCodeGroupRelationFormMap.put("nc_code_id", ncCodeId);
		List<NcCodeGroupRelationFormMap> listNcCodeGroupRelationFormMap = new ArrayList<>();
		listNcCodeGroupRelationFormMap = baseMapper.findByNames(ncCodeGroupRelationFormMap);
		
		//2.2如果有不良代码组，再查询不良代码组的详细信息
		if(ListUtils.isNotNull(listNcCodeGroupRelationFormMap)) {
			NcCodeGroupRelationFormMap map = listNcCodeGroupRelationFormMap.get(0);
			String ncCodeGroupId = map.getStr("nc_code_group_id");	
			List<NcCodeGroupFormMap> listNcCodeGroupFormMap = baseMapper.findByAttribute("id", ncCodeGroupId, NcCodeGroupFormMap.class);
			if (!ListUtils.isNotNull(listNcCodeGroupFormMap)) {
				log.error("未查询到不良代码组的详细信息，请确定该信息是否错误或被删除！");
				model.addAttribute("errorMessage", "未查询到不良代码组的详细信息，请确定该信息是否错误或被删除！");
				handlePageRes(model,request);
				return ncCodeMaintenanceUrl;
			}
			
			model.addAttribute("nc_code_group_id", ncCodeGroupId);
			model.addAttribute("nc_code_group_no", listNcCodeGroupFormMap.get(0).getStr("nc_code_group_no"));
		}
        //3.查询自定义数据
		getUDefinedDataValue(model,ncCodeId,uDefinedDataType,"dataKeys");
		
		model.addAttribute("successMessage", "检索成功");
		handlePageRes(model,request);
		return ncCodeMaintenanceUrl;
	}
	
	/**
	 * 删除不良代码
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delNcCode")
	public String delNcCode(Model model, HttpServletRequest request) {
		String ncCodeId = request.getParameter("nc_code_id");
		try {
			if (StringUtils.isEmpty(ncCodeId)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			ncCodeService.delNcCode(ncCodeId);
		} catch (BusinessException e) {
			String errMessage = e.getMessage();
			log.error(errMessage);
			model.addAttribute("res", findByRes());
			model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
			model.addAttribute("errorMessage", errMessage);
			handlePageRes(model,request);
			return ncCodeMaintenanceUrl;
		}
		model.addAttribute("res", findByRes());
		model.addAttribute("dataKeys", getUDefinedDataField(uDefinedDataType));
		model.addAttribute("successMessage", "删除不良代码成功");
		handlePageRes(model,request);
		return ncCodeMaintenanceUrl;
	}

}