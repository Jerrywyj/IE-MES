package com.iemes.controller.workcenter_model;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/workcenter_model/")
public class WorkcenterModelController extends BaseController {
	
	//页面跳转
	@RequestMapping("site_maintenance")
	public String Get_site_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/site/site_maintenance";
	}
	
	@RequestMapping("work_resource_type_maintenance")
	public String Get_work_resource_type_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/work_resource_type/work_resource_type_maintenance";
	}
	
	@RequestMapping("work_resource_maintenance")
	public String Get_work_resource_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/work_resource/work_resource_maintenance";
	}

	@RequestMapping("item_maintenance")
	public String Get_item_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/item/item_maintenance";
	}
	
	@RequestMapping("item_bom_maintenance")
	public String Get_item_bom_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/item_bom/item_bom_maintenance";
	}
	
	@RequestMapping("process_workflow_maintenance")
	public String Get_process_workflow_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/process_workflow/process_workflow_maintenance";
	}


	@RequestMapping("nc_code_maintenance")
	public String Get_nc_code_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/nc_code/nc_code_maintenance";
	}
	
	@RequestMapping("nc_group_maintenance")
	public String Get_nc_group_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/nc_group/nc_group_maintenance";
	}
	
	@RequestMapping("container_manager")
	public String Get_container_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/container/container_manager";
	}
	
	@RequestMapping("data_collection")
	public String Get_data_collection_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/data_collection/data_collection";
	}
	
	@RequestMapping("shift_maintenance")
	public String Get_shift_maintenance_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/shift/shift_maintenance";
	}

	@RequestMapping("number_rule_manager")
	public String Get_number_rule_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return Common.BACKGROUND_PATH + "/workcenter_model/number_rule/number_rule_manager";
	}
}