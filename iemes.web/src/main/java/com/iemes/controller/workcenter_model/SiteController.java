package com.iemes.controller.workcenter_model;


import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.SiteFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.site.SiteMapper;
import com.iemes.service.workcenter_model.SiteService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;

/**
 * 
 * @author mds
 * @Email: mds
 * @version 2.0v
 */
@Controller
@RequestMapping("/workcenter_model/site")
public class SiteController extends BaseController {
	
	@Inject
	private SiteMapper siteMapper;
	
	@Inject
	private SiteService siteService;
	
	private Logger log = Logger.getLogger(this.getClass());
	private String siteMaintenanceUrl = Common.BACKGROUND_PATH + "/workcenter_model/site/site_maintenance";
	
	//页面跳转
	@RequestMapping("site_maintenance")
	public String Get_site_maintenance_Url(Model model,HttpServletRequest request) throws Exception {
		handlePageRes(model,request);
		return siteMaintenanceUrl;
	}
	
	//保存
	@ResponseBody
	@RequestMapping("saveSite")
	@SystemLog(module="站点维护",methods="站点维护-保存站点")//凡需要处理业务逻辑的.都需要记录操作日志
	public String saveSite(){
		SiteFormMap siteFormMap = getFormMap(SiteFormMap.class);
		siteFormMap.put("by_user", ShiroSecurityHelper.getCurrentUsername());
		siteFormMap.put("create_time", DateUtils.getStringDateTime());
		siteFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		
		try {
			siteService.saveSite(siteFormMap);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	//检索
	@RequestMapping("querySiteDataBySiteCode")
	public String querySiteDataBySiteCode(Model model,HttpServletRequest request) {
		try {		
			SiteFormMap siteFormMap = new SiteFormMap();
			String site = request.getParameter("site");
			siteFormMap.put("site", site);
			
			List<SiteFormMap> listSiteFormMap = siteMapper.findByNames(siteFormMap);
			if (!ListUtils.isNotNull(listSiteFormMap)) {
				String errMsg = "未检索到站点为：" + site.toUpperCase() + "的信息";
				log.error(errMsg);
				model.addAttribute("errorMessage", errMsg);
				handlePageRes(model,request);
				return siteMaintenanceUrl;
			}
			siteFormMap = listSiteFormMap.get(0);
			handlePageRes(model,request);
			model.addAttribute("siteFormMap", siteFormMap);
			model.addAttribute("successMessage", "检索成功");
			
			return siteMaintenanceUrl;
		} catch (Exception e) {
			String errMsg = e.getMessage();
			log.error(errMsg);
			model.addAttribute("errorMessage", errMsg);
			handlePageRes(model,request);
			return siteMaintenanceUrl;
		}

	}
	
	/**
	 * 删除站点
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delSite")
	public String delSite(Model model,HttpServletRequest request) {
		String id = request.getParameter("site_id");
		try {
			if (StringUtils.isEmpty(id)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			siteService.delSite(id);
		}catch(BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			handlePageRes(model,request);
			return siteMaintenanceUrl;
		}
		handlePageRes(model,request);
		model.addAttribute("successMessage", "删除站点信息成功");
		return siteMaintenanceUrl;
	}
}