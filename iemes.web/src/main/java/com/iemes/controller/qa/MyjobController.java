package com.iemes.controller.qa;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.util.Common;


@Controller
@RequestMapping("/qa/myjob/")
public class MyjobController extends BaseController {
	
	private String backUrl = Common.BACKGROUND_PATH + "/qa/myjob/myjob";
	
	@RequestMapping("list")
	public String myJobList() {
		return backUrl;
	}

}
