package com.iemes.controller.work_process;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iemes.controller.index.BaseController;
import com.iemes.entity.PodPanelFormMap;
import com.iemes.util.Common;
import com.iemes.util.ShiroSecurityHelper;

@Controller
@RequestMapping("/work_process/pod_operation/")
public class PodOperationController extends BaseController {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private final String podOperationUrl = Common.BACKGROUND_PATH + "/work_process/pod_opeartion/pod_operation_panel_list";

	@RequestMapping("pod_operation")
	public String pod_panel_maintenance(Model model) throws Exception {
		return podOperationUrl;
	}
	
	/**
	 * 检索生产操作员-面板
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryPodPanel")
	public String queryPodPanel(Model model,HttpServletRequest request) {
		String podPanelNo = request.getParameter("pod_panel_no");
		try {
			//查询生产操作员-面板
			PodPanelFormMap podPanelFormMap = new PodPanelFormMap();
			podPanelFormMap.put("pod_panel_no", "%"+podPanelNo+"%");
			podPanelFormMap.put("site_id", ShiroSecurityHelper.getSiteId());
			List<PodPanelFormMap> list = baseMapper.findByNames(podPanelFormMap);
			
			model.addAttribute("podPanelNo", podPanelNo);
			model.addAttribute("listPodPanelFormMap", list);
		}catch(Exception e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return podOperationUrl;
		}
		model.addAttribute("successMessage", "检索成功");
		return podOperationUrl;
	}
	
	
}
