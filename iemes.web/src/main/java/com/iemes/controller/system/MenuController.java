package com.iemes.controller.system;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iemes.annotation.SystemLog;
import com.iemes.controller.index.BaseController;
import com.iemes.entity.ButtomFormMap;
import com.iemes.entity.Params;
import com.iemes.entity.ResFormMap;
import com.iemes.entity.ResRoleFormMap;
import com.iemes.entity.SiteFormMap;
import com.iemes.exception.BusinessException;
import com.iemes.mapper.ResourcesMapper;
import com.iemes.service.system.MenuService;
import com.iemes.util.Common;
import com.iemes.util.DateUtils;
import com.iemes.util.ListUtils;
import com.iemes.util.ResponseHelp;
import com.iemes.util.ShiroSecurityHelper;
import com.iemes.util.TreeObject;
import com.iemes.util.TreeUtil;
import com.iemes.util.UUIDUtils;

/**
 * 

 */
@Controller
@RequestMapping("/system/menu/")
public class MenuController extends BaseController {
	@Inject
	private ResourcesMapper resourcesMapper;

	@Inject
	private MenuService menuService;
	
	private Logger log = Logger.getLogger(this.getClass());
    private String menuManagerUrl = Common.BACKGROUND_PATH + "/system/menu/menu_manager";
	
	//页面跳转
	@RequestMapping("menu_manager")
	public String Get_menu_manager_Url(Model model) throws Exception {
		model.addAttribute("res", findByRes());
		return menuManagerUrl;
	}
	
	// 保存
	@ResponseBody
	@RequestMapping("saveMenu")
	@SystemLog(module = "菜单管理", methods = "菜单管理-保存菜单") // 凡需要处理业务逻辑的.都需要记录操作日志
	public String save() {
		ResFormMap resFormMap = getFormMap(ResFormMap.class);
		resFormMap.put("newId", UUIDUtils.getUUID());
		resFormMap.put("create_user", ShiroSecurityHelper.getCurrentUsername());
		resFormMap.put("create_time", DateUtils.getStringDateTime());

		try {
			menuService.saveMenu(resFormMap);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			return ResponseHelp.responseErrorText(e.getMessage());
		}
		return ResponseHelp.responseText();
	}
	
	/**
	 * 检索菜单
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("queryMenuData")
	public String queryMenuData(Model model, HttpServletRequest request) {
		try {
			ResFormMap resFormMap = new ResFormMap();
			String menu_key = request.getParameter("menu_key");
			resFormMap.put("res_key", menu_key);

			List<ResFormMap> listResFormMap = resourcesMapper.findByNames(resFormMap);
			if (!ListUtils.isNotNull(listResFormMap)) {
				String errMsg = "未检索到菜单标识为：" + menu_key.toUpperCase() + "的信息";
				log.error(errMsg);
				model.addAttribute("errorMessage", errMsg);
				return menuManagerUrl;
			}
			resFormMap = listResFormMap.get(0);
			model.addAttribute("resFormMap", resFormMap);
			model.addAttribute("successMessage", "检索成功");
			return menuManagerUrl;
		} catch (Exception e) {
			String errMsg = e.getMessage();
			log.error(errMsg);
			model.addAttribute("errorMessage", errMsg);
			return menuManagerUrl;
		}
	}
		
	/**
	 * 删除菜单
	 * 
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping("delMenu")
	public String delSite(Model model, HttpServletRequest request) {
		String id = request.getParameter("menu_id");
		try {
			if (StringUtils.isEmpty(id)) {
				throw new BusinessException("操作失败，参数不能为空！");
			}
			menuService.delMenu(id);
		} catch (BusinessException e) {
			log.error(e.getMessage());
			model.addAttribute("errorMessage", e.getMessage());
			return menuManagerUrl;
		}
		model.addAttribute("successMessage", "删除菜单信息成功");
		return menuManagerUrl;
	}
	
	//获取菜单目录
	@ResponseBody
	@RequestMapping("menu_list")
	public List<TreeObject> menu_list(Model model) throws Exception {
		ResFormMap resFormMap = getFormMap(ResFormMap.class);
		resFormMap.put("where", " where 1=1 order by level asc");
		List<ResFormMap> mps = resourcesMapper.findByWhere(resFormMap);
		List<TreeObject> list = new ArrayList<TreeObject>();
		for (ResFormMap map : mps) {
			TreeObject ts = new TreeObject();
			Common.flushObject(ts, map);
			list.add(ts);
		}
		TreeUtil treeUtil = new TreeUtil();
		List<TreeObject> ns = treeUtil.getChildTreeObjects(list, "0", "　");
		return ns;
	}

}