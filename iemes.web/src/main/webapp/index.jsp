<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>IE-MES制造执行系统</title>
	<%@include file="/js/common/common.jspf"%>
	<link rel="stylesheet" href="css/index/index.css" type="text/css" />
	<script type="text/javascript" src="js/index/index.js"> </script>
</head>
<body>
	<div class="index">
		<div class="index_header">
			<div class="index_header_tittle">IE-MES</div>
			<div class="index_header_right"> 
			  <a href="#" id="main">主页</a>
			  <a href="#" id="updatePass">${userFormMap.account_name}</a>
			  <a href="#" id="logout">${site}</a>
			  <a href="#" id="about">关于</a>
			  <!-- <a href="#">帮助</a>	 -->
			  <label>迈鼎盛科技</label>
			</div>
		</div>
		<div class="index_subheader">
			<span> 欢迎使用IE-MES v2.0 </span>
			<a class="iedex_subheader_sop" resources_id="" resources_name="" href="#" id="sop">SOP</a>
		</div>
		<div class="index_menu">
		
		</div>
		
		<div id="loadhtml" class="index_centent">
					 
			
		</div>
		
		<!-- 页脚 -->  

        <div class="index_footer">      
		  <span>© 2018 www.maidingsheng.net 版权所有</span>	
		</div>
	</div>
</body>
</html>