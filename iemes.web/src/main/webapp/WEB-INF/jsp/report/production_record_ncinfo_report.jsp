<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="/js/common/common.jspf"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>SFC不良信息列表</title>

<script type="text/javascript" src="${ctx }/js/common/form_validate.js"></script>
<script type="text/javascript" src="${ctx }/js/common/jtab.js"></script>
<script type="text/javascript" src="${ctx }/js/common/mtable.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>

		<div id="production_record_report">
			<div class="openPopup_title">SFC不良信息列表</div>
			<table class="mtable" id="production_record_flowstep_table">
				<tr>
					<th data-value="sfc">产品SFC</th>
					<th data-value="workshop_no">车间</th>
					<th data-value="workline_no">产线</th>
					<th data-value="operation_no">操作</th>
					<th data-value="work_resource_no">资源</th>
					<th data-value="nc_code_group">不良代码组</th>
					<th data-value="nc_code">不良代码</th>
					<th data-value="dispose_info">处置信息</th>
					<th data-value="dispose_type">处置类型</th>
					<th data-value="remark">备注</th>
				</tr>
				<c:forEach var="data" items="${list }">
					<tr>
						<td>${data.sfc }</td>
						<td>${data.workshop_no }</td>
						<td>${data.workline_no }</td>
						<td>${data.operation_no }</td>
						<td>${data.work_resource_no }</td>
						<td>${data.nc_code_group }</td>
						<td>${data.nc_code }</td>
						<td>${data.dispose_info }</td>
						<td>${data.dispose_type }</td>
						<td>${data.remark }</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</form>
</body>
</html>