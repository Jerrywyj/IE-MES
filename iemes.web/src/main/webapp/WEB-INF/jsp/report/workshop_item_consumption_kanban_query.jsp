<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta charset="utf-8">
<link rel="shortcut icon"
	href="${pageContext.servletContext.contextPath }/images/favicon.png"
	type="image/x-icon" />
<title>车间物料消耗看板查询</title>
	<script type="text/javascript" src="js/report/workshop_item_consumption_kanban_query.js"></script>	
</head>

<body class="report_body" >	
<form class="mds_tab_form">
    <div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<input type="button" id="btnQuery" value="检索"> 
			<input type="button" id="btnClear" value="清除"> 
	</div>	
	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>
		<div class="report_header">
			<div class="formField_query">
				<label class="must">站点:</label> <span>${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">车间:</label> 
				<input type="text"
					dataValue="workcenter_no"
					viewTitle="车间"
					relationId="input_workshop_id"
					data-url="/popup/queryWorkCenter.shtml" 
					class="formText_query" id="tbx_workshop_no"/>
				<input type="hidden" id="input_workshop_id" validate_allowedempty="N" validate_errormsg="车间不能为空！" relationId= "tbx_workshop_version"
				       dataValue="id" name="workshopInventoryFormMap.workshop_id" >	
				<input type="text" class="form_version majuscule" placeholder="版本" id="tbx_workshop_version"
				       dataValue="workcenter_version">
				<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workshop_no">
   		  </div>
   		</div> 
</form>
</body>
</html>
