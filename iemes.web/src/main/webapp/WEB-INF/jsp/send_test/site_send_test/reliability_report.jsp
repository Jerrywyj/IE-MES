<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>可靠性报表</title>
<%@include file="/js/common/common.jspf"%>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
</head>
<body>
	<form class="mds_tab_form">
		<div class="hidden">
			<input type="hidden" name="formMap.reportMethod" value="getProductionRecordList"> 
			<input type="hidden" value="${errorMessage}" id="errorMessage"> 
			<input type="hidden" value="${successMessage}" id="successMessage">
		</div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
			<label class="mds_message_label"></label> <span
				class="iemes_style_NoticeButton"> <input type="button"
				submit="N" onclick="closeNoticeMessage();" value="关闭">
			</span>
		</div>
	
		<div id="production_record_report">	
			<div class="openPopup_title">
				基础信息
			</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="sfc">客户</th>
					<th data-value="sfc_status">机型</th>
					<th data-value="main_item">品名</th>
					<th data-value="shoporder_status">颜色</th>
					
					<th data-value="sfc">生产日期</th>
					<th data-value="sfc">送测日期</th>
					<th data-value="shoporder_create_time">送测数量</th>
					<th data-value="shoporder_create_time">料号</th>
					
					<th data-value="sfc">油漆类型</th>
					<th data-value="sfc">油漆供应商</th>
					<th data-value="shoporder_create_time">送验人</th>
					<th data-value="shoporder_create_time">联系电话</th>					
				</tr>
				<tr>
					<td>与德</td>
					<td>A165/E200/Z350</td>
					<td>底壳</td>
					<td>黑</td>
					
					<td>2017/2/20</td>
					<td>2017/2/20</td>
					<td>5</td>
					<td></td>
					
					<td></td>
					<td></td>
					<td>谢桂丽</td>
					<td></td>
				</tr>
			</table>
			<div class="openPopup_title">
				审批信息
			</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="sfc">最终判定</th>
					<th data-value="sfc_status">测试人</th>
					<th data-value="main_item">审核</th>
					<th data-value="shoporder_status">核准</th>
				</tr>
				<tr>
					<td>合格</td>
					<td>黄雪</td>
					<td></td>
					<td>戴卫娟</td>
				</tr>
			</table>
			<div class="openPopup_title">
			测试项目：常规测试，环境测试
			</div>
			<table class="mtable" id="production_record_table">
				<tr>
					<th data-value="shoporder_id" class="mtable_head_hide"></th>
					<th data-value="sfc">检验项目</th>
					<th data-value="sfc_status">测试方法</th>
					<th data-value="main_item">测试结果</th>
					<th data-value="shoporder_status">使用工具</th>				
					<th data-value="sfc">备注</th>				
				</tr>
				<tr>
					<td>硬度测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>硬度测试仪</td>
					<td>
					  
					</td>
				</tr>
				
				<tr>
					<td>耐磨测试</td>
					<td>
					试样固定放置在RCA耐磨仪(型号:7-IBB),样品必须贴紧不能移动或偏移或组合操作杆弹起,把计数器归零,<br/>
					设置所需次数进行测试,大面积部位和凌角部位均需安排试验,试验过程中每50次检查一次,用荷重175g法码，<br/>
					UV漆300次(85次不得出现明显拉痕.表面丝印100次,底漆丝印200次);<br/>
					PU漆/电镀150次(丝印100次);UV手感漆200次(丝印80次);橡胶漆/绒毛漆50次(丝印50次);PU手感漆50次(丝印50次);<br/>
					阳极氧化300次(丝印150次);五金电泳50次(丝印50次);试验完成后油漆不见底材,丝印不被磨损为合格.<br/>
					</td>
					<td>
					  OK
					</td>
					<td>RCA耐磨仪</td>
					<td>
					 
					</td>
				</tr>
				<tr>
					<td>耐醇测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>耐醇测试仪</td>
					<td>
					  
					</td>
				</tr>
				<tr>
					<td>附着力测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>百格刀/3M胶纸</td>
					<td>
					  
					</td>
				</tr>
				<tr>
					<td>水煮测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>恒温水浴锅</td>
					<td>
					  
					</td>
				</tr>
				<tr>
					<td>落锤测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>测试仪</td>
					<td>
					  
					</td>
				</tr>
				<tr>
					<td>弯折测试</td>
					<td>
					用三菱牌铅笔,UV漆2H;电镀1H;PU漆/橡胶漆/手感漆/绒毛漆HB;<br/>
					电泳/阳极氧化/五金烤漆F;将样品的涂膜面朝上水平放置在试验台上且固定,<br/>
					在砂纸上磨平铅笔笔芯,使铅笔尖与漆面成45度夹角,在硬度测试仪上荷重1000g;<br/>
					高光UV漆2H荷重750g,在待测样品表面找一个原点,以此为中心的5个不同方向位置各划一次,<br/>
					行程5mm测试,用橡皮擦拭后50cm目视外观不得出现刮痕及划破表面层烤漆膜达到底层或下层烤漆膜现象。
					</td>
					<td>
					  OK
					</td>
					<td>测试仪</td>
					<td>
					  
					</td>
				</tr>
			</table>
		</div>
	</form>

</body>
</html>