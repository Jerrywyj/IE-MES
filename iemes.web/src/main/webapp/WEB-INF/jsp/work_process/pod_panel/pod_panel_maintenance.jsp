<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生产操作员-面板维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_panel/pod_panel_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
		<input type="button" id="btnDelete" value="删除">
	</div>
	
	<div class="hidden">
		<input type="hidden" name="podPanelFormMap.id" value="${podPanelFormMap.id}" id="pod_panel_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">生产操作员面板编号:</label> 
			<input type="text"
				dataValue="pod_panel_no" 
				viewTitle="POD面板"
				data-url="/popup/queryAllPodPanel.shtml" 
				class="formText_query majuscule" id="tbx_pod_panel_no" validate_allowedempty="N" validate_errormsg="请输入生产操作员面板编号！" 
				name="podPanelFormMap.pod_panel_no" value="${podPanelFormMap.pod_panel_no }"/>
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_panel_no">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_3tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">按钮</a></li>
		<li><a href="#" tabid="tab3">选项</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label>生产操作员面板名称:</label> <input type="text" validate_allowedempty="N" validate_errormsg="请输入生产操作员面板名称！" 
						class="formText_content" id="tbx_pod_panel_name" name="podPanelFormMap.pod_panel_name" value="${ podPanelFormMap.pod_panel_name}"/>
				</div>
				<div class="formField_content">
					<label>描述:</label> <input type="text"
						class="formText_content" id="tbx_pod_panel_des" name="podPanelFormMap.pod_panel_desc" value="${podPanelFormMap.pod_panel_desc }"/>
				</div>
				<div class="formField_content">
					<label>状态:</label> 
					<select class="formText_content"  id="select_pod_panel_status" name="podPanelFormMap.status">
                    <c:if test="${podPanelFormMap.status==-1 }">
                    	<option value="1">已启用</option>
                   		<option value="-1" selected="selected">已禁用</option>
                    </c:if>
                    <c:if test="${podPanelFormMap.status!=-1 }">
                    	<option value="1" selected="selected">已启用</option>
                   		<option value="-1">已禁用</option>
                    </c:if>
                    </select>			
	            </div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${podPanelFormMap.create_user }</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${podPanelFormMap.create_time }</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
			<div class="bt_div">
				<a class="bt_a" id="tb_add">插入新行</a>
				<a class="bt_a" id="tb_del">删除选定行</a>
				<a class="bt_a" id="tb_delAll">删除全部</a>
			</div>
			<table class="mtable" id="pod_panel_botton_table">
				<tr>
					<th class="must" data-value="pod_botton_level">顺序</th>
					<th class="must" data-value="pod_botton_no">按钮编号</th>
					<th class="must" data-value="pod_botton_name">按钮名称</th>
					<th class="must" data-value="pod_botton_user">创建人</th>
				</tr>
				<c:forEach var="button" items="${listPodPanelButtonFormMap}" varStatus="status">
					<tr>
						<td>
					        <input type="text" readonly="readonly" validate_datatype="number_int" value="${button.pod_panel_button_level }"
					        validate_errormsg="顺序必须为数字" value="${status.count*10 }">
					    </td>
					    <td>
					        <input type="text" class="formText_query majuscule" 
					        	datavalue="pod_button_no" value="${button.pod_button_no }"
						        viewtitle="POD按钮" 
						        data-url="/popup/queryAllPodButtons.shtml" 
						        relationid="popup_pod_button_id${status.count*10 }"
						        id="popup_pod_button_no${status.count*10 }" validate_allowedempty="N" validate_errormsg="POD按钮不能为空！">
					        <input type="hidden" id="popup_pod_button_id${status.count*10 }" datavalue="id" relationid="popup_pod_button_name${status.count*10 }" value="${button.pod_button_id }">
					        <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textfieldid="popup_pod_button_no">
					    </td>
					    <td>
					        <input type="text" readonly="readonly" validate_allowedempty="N" id="popup_pod_button_name${status.count*10 }" value=${button.pod_button_name }
					        datavalue="pod_button_name" validate_errormsg="POD名称不能为空！" relationid="popup_pod_buttion_create_user${status.count*10 }">
					    </td>
					    <td>
					        <input type="text" readonly="readonly" validate_allowedempty="N" id="popup_pod_buttion_create_user${status.count*10 }" value="${button.user }"
					        datavalue="create_user" validate_errormsg="创建人不能为空！">
					    </td>
					</tr>
				</c:forEach>
			</table>
		</div>		
		<div id="tab3">
			<div class="inputForm_content">
			    <div class="formField_content">
					<label>缺省操作:</label> 
					<input type="text"
						dataValue="operation_no" 
						relationId="input_operation_id"
						viewTitle="操作"
						data-url="/popup/queryAllOperation.shtml" 
						class="formText_content" id="tbx_pod_default_operation" value="${operationFormMap.operation_no }"/>
					<input type="hidden" id="input_operation_id" dataValue="id" name="podPanelFormMap.default_operation" value="${podPanelFormMap.default_operation}">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_default_operation">
				</div>
				<div class="formField_content">
					<label>缺省资源:</label> 
					<input type="text"
						dataValue="resource_no" 
						viewTitle="资源"
						relationId="input_resource_id"
						data-url="/popup/queryAllResource.shtml"
						class="formText_content" id="tbx_pod_default_work_resource" value="${workResourceFormMap.resource_no }"/>
					<input type="hidden" id="input_resource_id" dataValue="id" name="podPanelFormMap.default_resource" value="${podPanelFormMap.default_resource}">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_pod_default_work_resource">
				</div>
			    <div class="formField_content">
					<label>操作可以更改:</label> 
					<select class="formText_content"  id="select_operation_canchange" name="podPanelFormMap.changeOperation">
					<c:if test="${podPanelFormMap.changeOperation==-1 }">
						<option value="1">是</option>
                    	<option value="-1" selected="selected">否</option>
					</c:if>
                    <c:if test="${podPanelFormMap.changeOperation!=-1 }">
						<option value="1" selected="selected">是</option>
                    	<option value="-1">否</option>
					</c:if>
                    </select>			
	            </div>
	            <div class="formField_content">
					<label>资源可以更改:</label> 
					<select class="formText_content"  id="select_work_resource_canchange" name="podPanelFormMap.changeResource">
					<c:if test="${podPanelFormMap.changeResource==-1 }">
						<option value="1">是</option>
                    	<option value="-1" selected="selected">否</option>
					</c:if>
					<c:if test="${podPanelFormMap.changeResource!=-1 }">
						<option value="1" selected="selected">是</option>
                    	<option value="-1">否</option>
					</c:if>
                    </select>			
	            </div>			
			</div>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>