<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>维修界面</title>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_operation/pod_repair_maintenance.js"></script>
</head>
<body>
	<div id="mds_form_content">
		<form id="repair_form" >
			<div class="hidden_div">
				<input type="hidden" name="ncRepairFormMap.sfc" value="${assembleMap.sfc }" id="sfc">
				<input type="hidden" name="ncRepairFormMap.process_workflow_id" value="${assembleMap.process_workflow_id }" id="process_workflow_id">
				<input type="hidden" name="ncRepairFormMap.sfc_step_id" value="${assembleMap.id }" id="sfc_step_id">
				<input type="hidden" name="ncRepairFormMap.item_id" value="${assembleMap.item_sfc_id }" id="item_sfc_id">
				<input type="hidden" name="ncRepairFormMap.main_item_id" value="${assembleMap.main_item_id }" id="main_item_id">
				<input type="hidden" name="ncRepairFormMap.workshop_id" value="${assembleMap.workshop_id }" id="workshop_id">
				<input type="hidden" name="ncRepairFormMap.workline_id" value="${assembleMap.workline_id }" id="workline_id">
				<input type="hidden" name="ncRepairFormMap.operation_id" value="${assembleMap.operation_id }" id="operation_id">
				<input type="hidden" name="ncRepairFormMap.work_resource_id" value="${assembleMap.work_resource_id }" id="work_resource_id">
				<input type="hidden" name="ncRepairFormMap.shoporder_id" value="${assembleMap.shoporder_id }" id="shoporder_id">
				<input type="hidden" name="ncRepairFormMap.use_num" value="${assembleMap.use_num }" id="use_num">
			</div>
			<div class="assemble_from_tr">
				<div class="formline_panel_ext">
					<label class="assemble_form_label">车间作业控制：</label>
					<input type="text" class="assemble_form_input_ext" value="${assembleMap.sfc }"
					name="ncRepairFormMap.sfc">
				</div>
			</div>
			<div class="assemble_from_tr_60">
				<div class="formline_panel">
					<label class="assemble_form_label">不良代码组：</label>
					<input type="text" value="${sfcNcFormMap.nc_code_group }">
					<input type="hidden" name="ncRepairFormMap.nc_code_group_id" value="${sfcNcFormMap.nc_code_group_id }">
				</div>
				<div class="formline_panel">
					<label class="assemble_form_label">选择步骤：</label>
					<select name="ncRepairFormMap.next_operation">
						<c:forEach var="operation" items="${nextOperations }">
							<option value="${operation.next_operation_id }">${operation.operation_no }</option>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="assemble_from_tr_60">
				<div class="formline_panel">
					<label class="assemble_form_label">不良代码：</label>
					<input type="text" value="${sfcNcFormMap.nc_code }">
					<input type="hidden" name="ncRepairFormMap.nc_code_id" value="${sfcNcFormMap.nc_code_id }">
				</div>
				<div class="formline_panel">
					<label class="assemble_form_label">备注：</label>
					<textarea name="ncRepairFormMap.repair_desc" rows="3" cols="40"></textarea>
				</div>
			</div>
			<div class="assemble_from_tr_ext">
				<input type="submit" value="维修" class="assemble_button" id="bt_assemble">
				<input type="reset" value="清空" class="assemble_button">
			</div>
		</form>
	</div>
</body>
</html>