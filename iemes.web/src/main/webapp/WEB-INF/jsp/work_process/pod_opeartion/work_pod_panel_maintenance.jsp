<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>生产操作员工作面板</title>

<link rel="stylesheet"
	href="css/work_process/pod_opeartion/work_pod_panel_maintenance.css"
	type="text/css" />

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_operation/pod_operation.js"></script>

</head>
<body>
	<div class="work_pod_panel_body">
		<form class="mds_tab_form">
			<div class="hidden">
				<input type="hidden" name="podPanelFormMap.id" value="${id}" id="pod_panel_id"> 
				<input type="hidden" value="${errorMessage}" id="errorMessage"> 
				<input type="hidden" value="${successMessage}" id="successMessage">
			</div>

			<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
				<label class="mds_message_label"></label> 
				<span class="iemes_style_NoticeButton"> 
					<input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭">
				</span>
			</div>

			<div class="gradual_change_panel">生产操作员面板选择</div>
			<div class="work_pod_query content_search_button">
				<div class="work_pod_query_tr">
					<div class="work_pod_query_left">
						<label class="must work_pod_label">操作:</label> 
						<input type="text"
							dataValue="operation_no" 
							callBackFun="getSfcOnOperation"
							relationId="input_operation_id"
							viewTitle="操作"
							data-url="/popup/queryPodOperation.shtml" 
							class="formText_query work_pod_input" id="tbx_pod_operation" value="${operationNo }"
							validate_errormsg="操作不能为空！！！"/> 
						<input type="hidden" id="input_operation_id" validate_errormsg="操作不能为空！！！" dataValue="id" relationId="tbx_pod_work_resource" value="${operationId }">
						<input type="button" submit="N" value="检索"
							onclick="operationBrowse(this)" textFieldId="tbx_pod_operation">
					</div>
					<div class="work_pod_query_right">
						<label class="must work_pod_label">资源:</label> 
						<input type="text"
							dataValue="resource_no" 
							filterId="tbx_pod_operation,input_operation_id"
							callBackFun="getSfcOnOperation"
							viewTitle="资源"
							data-url="/popup/queryPodResource.shtml"
							class="formText_query work_pod_input" id="tbx_pod_work_resource" value="${workResourceNo }"/>
						<input type="hidden" id="input_resource_id" validate_errormsg="资源不能为空！！！" dataValue="resource_id" value="${workResourceId }">
						<input type="button" submit="N" value="检索"
							onclick="operationBrowse(this)" textFieldId="tbx_pod_work_resource">
					</div>
				</div>
				<div class="work_pod_query_tr">
					<div class="work_pod_query_left">
						<label class="work_pod_label">车间作业控制:</label> 
						<input type="text"
							filterId="input_operation_id,input_resource_id"
							dataValue="sfc" 
							beforFun="getWorkLineByResource"
							viewTitle="SFC"
							data-url="/popup/queryOperationSfcByWorkResource.shtml"
							class="formText_query work_pod_input" id="tbx_sfc_no"
							name="podPanelFormMap.pod_panel_no" value="${podPanelNo }" /> 
						<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_sfc_no">
					</div>
				</div>
			</div>
			<div class="solid_panel">
				<c:forEach var="botton" items="${listButton }">
					<input type="button" value="${botton.pod_button_name }" class="pod_button" onclick="" id="${botton.pod_function_url}">
				</c:forEach>
			</div>
			<div class="pod_controller_panel">
				<div class="gradual_change_panel">生产操作员面板工作列表</div>
				<table class="mtable" id="pod_controller_table">
					<tr>
						<th data-value="id" class="mtable_head_hide"></th>
						<th data-value="process_workflow_id" class="mtable_head_hide"></th>
						<th data-value="operation_id" class="mtable_head_hide"></th>
						<th data-value="work_resource_id" class="mtable_head_hide"></th>
						<th data-value="item_id" class="mtable_head_hide"></th>
						<th data-value="workshop_id" class="mtable_head_hide"></th>
						<th data-value="workline_id" class="mtable_head_hide"></th>
						<th data-value="shift_id" class="mtable_head_hide"></th>
						<th data-value="shoporder_id" class="mtable_head_hide"></th>
						<th data-value="shoporder_sfc_id" class="mtable_head_hide"></th>
						<th data-value="operation_no" class="mtable_head_hide"></th>
						<th data-value="resource_no" class="mtable_head_hide"></th>
						<th width="3%" data-value="level">顺序</th>
						<th data-value="sfc">车间作业控制</th>
						<th data-value="item">物料</th>
						<th data-value="num">数量</th>
						<th data-value="workshop">车间</th>
						<th data-value="workline">产线</th>
						<th data-value="shift">班次</th>
						<th data-value="shoporder">工单</th>
						<th data-value="status">操作状态</th>
						<th data-value="create_time">开始操作时间</th>
					</tr>
					<c:if test="${sfcList==null || sfcList.size()<=0 }">
						<c:forEach var="s" begin="1" end="10">
							<tr>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</c:forEach>
					</c:if>
					<c:if test="${sfcList!=null && sfcList.size()>0}">
						<c:forEach var="sfcStepFormMap" items="${sfcList}" varStatus="status">
							<tr>
								<td class="mtable_head_hide">${sfcStepFormMap.id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.process_workflow_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.operation_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.work_resource_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.item_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.workshop_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.workline_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.shift_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.shoporder_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.shoporder_sfc_id }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.operation_no }</td>
								<td class="mtable_head_hide">${sfcStepFormMap.resource_no }</td>
								<td>${status.count}</td>
								<td>${sfcStepFormMap.sfc}</td>
								<td>${sfcStepFormMap.item_no}</td>
								<td>1</td>
								<td>${sfcStepFormMap.workshop_no}</td>
								<td>${sfcStepFormMap.workline_no}</td>
								<td>${sfcStepFormMap.shift_no}</td>
								<td>${sfcStepFormMap.shoporder_no}</td>
								<c:if test="${sfcStepFormMap.status==1}">
									<td>生产中</td>
								</c:if>
								<c:if test="${sfcStepFormMap.status==2}">
									<td>已完成</td>
								</c:if>
								<td>${sfcStepFormMap.create_time}</td>
							</tr>
						</c:forEach>
						<c:forEach var="s" begin="1" end="${10-sfcList.size() }">
							<tr>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td class="mtable_head_hide"></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</c:forEach>
					</c:if>
				</table>
				<div class="pod_assemble_list_panel">
					<div class="gradual_change_panel" id="assemble_itemList_panel"><label id="pod_assemble_list_label">装配物料清单</label><input class="bt_pod_assemble" id="bt_pod_assemble_list" type="button" value="关闭"></div>
					<div id="pod_assemble_list">
						
					</div>
				</div>
				<div class="pod_assemble_panel">
					<div class="gradual_change_panel">装配作业面板<input class="bt_pod_assemble" id="bt_pod_assemble_panel" type="button" value="关闭"></div>
					<div id="pod_assemble_div">
						
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>