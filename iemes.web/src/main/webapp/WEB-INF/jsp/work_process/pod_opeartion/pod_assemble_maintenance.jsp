<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>装配</title>
<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>
<script type="text/javascript" src="js/work_process/pod_operation/pod_assemble_maintenance.js"></script>
</head>
<body>
	<div id="mds_form_content">
		<form id="assemble_form" >
			<div class="hidden_div">
				<input type="hidden" name="sfcAssemblyFormMap.item_id" value="${assembleMap.item_sfc_id }" id="item_sfc_id">
				<input type="hidden" name="sfcAssemblyFormMap.main_item_id" value="${assembleMap.main_item_id }" id="main_item_id">
				<input type="hidden" name="sfcAssemblyFormMap.workshop_id" value="${assembleMap.workshop_id }" id="workshop_id">
				<input type="hidden" name="sfcAssemblyFormMap.workline_id" value="${assembleMap.workline_id }" id="workline_id">
				<input type="hidden" name="sfcAssemblyFormMap.operation_id" value="${assembleMap.operation_id }" id="operation_id">
				<input type="hidden" name="sfcAssemblyFormMap.work_resource_id" value="${assembleMap.work_resource_id }" id="work_resource_id">
				<input type="hidden" name="sfcAssemblyFormMap.shoporder_id" value="${assembleMap.shoporder_id }" id="shoporder_id">
				<input type="hidden" name="sfcAssemblyFormMap.use_num" value="${assembleMap.use_num }" id="use_num">
				<input type="hidden" name="sfcAssemblyFormMap.item_save_type" value="${item_save_type }" id="item_save_type">
				
				<input type="hidden" value="${errorMessage}" id="errorMessage">
				<input type="hidden" value="${successMessage}" id="successMessage">
			</div>
			<div class="assemble_from_tr">
				<div class="formline_panel_ext">
					<label class="assemble_form_label">车间作业控制：</label>
					<input type="text" class="assemble_form_input_ext" value="${assembleMap.sfc }"
					name="sfcAssemblyFormMap.sfc">
				</div>
			</div>
			<div class="assemble_from_tr">
				<div class="formline_panel">
					<label class="assemble_form_label">物料编号：</label>
					<input type="text" class="assemble_form_input" value="${assembleMap.item_no }">
				</div>
				<div class="formline_panel">
					<label class="assemble_form_label">装配组件：</label>
					<c:if test="${item_save_type=='unit' }">
						<input type="text" class="assemble_form_input"
							dataValue="item_sfc"
							viewTitle="组件SFC"
							data-url="/popup/queryAllInventorySfc.shtml"
							filterId="item_sfc_id,main_item_id"
							id="assemble_item_input" name="sfcAssemblyFormMap.item_sfc"
							validate_allowedempty="N" validate_errormsg="装配组件不能为空！！！"
							>
						<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="assemble_item_input">
					</c:if>
					<c:if test="${item_save_type=='batch' }">
						<input type="text" class="assemble_form_input"
							dataValue="item_inner_batch"
							viewTitle="组件批次"
							data-url="/popup/queryAllInventoryBatch.shtml"
							filterId="item_sfc_id,main_item_id"
							id="assemble_item_input" name="sfcAssemblyFormMap.item_inner_batch"
							validate_allowedempty="N" validate_errormsg="装配组件不能为空！！！">
						<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="assemble_item_input">
					</c:if>
					
				</div>
			</div>
			<div class="assemble_from_tr">
				<div class="formline_panel">
					<label class="assemble_form_label">物料名称：</label>
					<input type="text" class="assemble_form_input" value="${assembleMap.item_name }" name="sfcAssemblyFormMap.item_name">
				</div>
				<div class="formline_panel">
					<label class="assemble_form_label">装配数量：</label>
					<c:if test="${item_save_type=='unit' }">
						<input type="Number" class="assemble_form_input" value="1" name="sfcAssemblyFormMap.use_number" readonly="readonly" id="use_number"
							validate_datatype="number_int" validate_errormsg="装配组件不能为空且必须是正整数！！！">
					</c:if>
					<c:if test="${item_save_type=='batch' }">
						<input type="Number" class="assemble_form_input" value="1" name="sfcAssemblyFormMap.use_number" id="use_number"
							validate_datatype="number_int" validate_errormsg="装配组件不能为空且必须是正整数！！！">
					</c:if>
				</div>
			</div>
			<div class="assemble_from_tr">
				<div class="formline_panel">
					<label class="assemble_form_label">物料描述：</label>
					<input type="text" class="assemble_form_input" value="${assembleMap.item_desc }">
				</div>
			</div>
			<div class="assemble_from_tr_ext">
				<input type="submit" value="装配" class="assemble_button" id="bt_assemble">
				<input type="reset" value="清空" class="assemble_button">
			</div>
		</form>
	</div>
</body>
</html>