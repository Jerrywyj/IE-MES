<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>车间作业步骤调整</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/work_scheduling/work_step_change/work_step_change.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
		<%-- <c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
	    </c:forEach> --%>
	    <!-- 最后再统一的改成动态按钮 -->
		<input type="button" id="btnQuery" value="检索"> 
		<input type="submit" id="btnSave"  value="保存"> 
		<input type="button" id="btnClear" value="清除"> 
	</div>
	
	<div class="hidden">
		<input type="hidden" name="shopOrderSfcFormMap.id" value="${shopOrderSfcFormMap.id}" id="sfc_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
    </div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">车间作业控制:</label>
			<input type="text"
			    dataValue="sfc"
				viewTitle="车间作业控制"
				data-url="/popup/queryAllSfc.shtml" 
				class="formText_query majuscule" id="tbx_sfc_no" name="shopOrderSfcFormMap.sfc" value="${shopOrderSfcFormMap.sfc}"
				validate_allowedempty="N" validate_errormsg="请输入车间作业控制！" />
		     <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_sfc_no">	
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_3tabs">
		<li><a href="#" tabid="tab1">调整</a></li>
		<li><a href="#" tabid="tab2">步骤列表</a></li>
		<li><a href="#" tabid="tab3">调整历史</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
			     <div class="formField_content">
					<label>sfc当前状态:</label> 
					<span class="formField_content">
					   <c:forEach var="sfc_status" items="${sfcStatusList}">
				          <c:if test="${shopOrderSfcFormMap.sfc_status==sfc_status.key}">${sfc_status.value}</c:if>
			           </c:forEach>
					</span>
				</div>
				<div class="formField_content">
					<label>当前步骤:</label> <span class="formField_content">${sfcStepFormMap.operation}</span>
				</div>
				<div class="formField_content">
					<label>当前步骤状态:</label>
					 <span class="formField_content">
					    <c:forEach var="step_status" items="${sfcStepStatusList}">
				          <c:if test="${sfcStepFormMap.status==step_status.key}">${step_status.value}</c:if>
			           </c:forEach>
					</span>
				</div>
				<div class="formField_content">
					<label>新的步骤:</label>
				    <select class="formText_content" id="select_new_opeartion"  name="shopOrderSfcFormMap.new_operation_id">
						<c:forEach var="data" items="${newflowStepFormMapList}">
						    <c:if test="${sfcStepFormMap.operation_id==data.operation_id}">
						       <option selected="selected" value="${data.operation_id}">${data.operation}</option>
						    </c:if>
							<c:if test="${sfcStepFormMap.operation_id!=data.operation_id}">
							   <option value="${data.operation_id}">${data.operation}</option>
							</c:if>
						</c:forEach>
						<option value="STEP_END">完成</option>
				 </select>
				</div>
				<div class="formField_content">
					<label>备注:</label> 
					<textarea type="text"
					    name="shopOrderSfcFormMap.remark"
						class="formText_content" id="tbx_remark" />
					</textarea>
				</div>
			</div>
		</div>
		 <div id="tab2">
	 		<table class="mtable" id="shoporder_status_change_history">
						<tr>
							<th>操作</th>
							<th>下个操作</th>
							<th>创建人</th>
							<th>创建时间</th>						
						</tr>
						<c:forEach var="data" items="${flowStepFormMapList}">
							<tr>
								<td>${data.operation}</td>
								<td>${data.next_operation}</td>
								<td>${data.create_user}</td>
								<td>${data.create_time}</td>
							</tr>
						</c:forEach>
			 </table>
		</div>	
		 <div id="tab3">
	 		<table class="mtable" id="sfc_step_change_history">
						<tr>
							<th>调整时间</th>
							<th>调整人</th>
							<th>调整备注</th>
							<th>调整前步骤</th>
							<th>调整前步骤状态</th>
							<th>调整后步骤</th>	
							<th>调整后步骤状态</th>					
						</tr>
						<c:forEach var="data" items="${stepChangeHistory}">
							<tr>
								<td>${data.change_time}</td>
								<td>${data.change_user}</td>
								<td>${data.change_remark}</td>
								<td>${data.sfcStepFormMapBeforeChange.operation}</td>
								<td>
									<c:forEach var="step_status" items="${sfcStepStatusList}">
					                <c:if test="${data.sfcStepFormMapBeforeChange.status==step_status.key}">${step_status.value}</c:if>
				                    </c:forEach>
								</td>
								<td>${data.sfcStepFormMapAfterChange.operation}</td>
								<td>
									<c:forEach var="step_status" items="${sfcStepStatusList}">
					                <c:if test="${data.sfcStepFormMapAfterChange.status==step_status.key}">${step_status.value}</c:if>
				                    </c:forEach>
								</td>
							</tr>
						</c:forEach>
			 </table>
		</div>
				
	</div>

	<!--  Tab -->
	</form>
</body>
</html>