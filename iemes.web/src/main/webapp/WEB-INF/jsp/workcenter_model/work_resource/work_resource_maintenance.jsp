<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>资源维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/workcenter_model/work_resource/work_resource_maintenance.js"></script>

</head>
<body>
<form class="mds_tab_form">
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
		<input type="hidden" name="workResourceFormMap.id" value="${workResourceFormMap.id}" id="work_resource_id">
		<input type="hidden" value="${errorMessage}" id="errorMessage">
		<input type="hidden" value="${successMessage}" id="successMessage">
	</div>

	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>

	<div class="inputForm_query">
		<div class="formField_query">
			<label class="must">站点:</label> <span class="formText_query">${site}</span>
		</div>
		<div class="formField_query">
			<label class="must">资源编号:</label> 
			<input type="text"
				dataValue="resource_no" 
				viewTitle="资源"
				data-url="/popup/queryAllResource.shtml"
				class="formText_query majuscule"   name="workResourceFormMap.resource_no" value="${workResourceFormMap.resource_no}"
				id="tbx_work_resource_no" validate_allowedempty="N" validate_errormsg="请输入资源编号！" />
			<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_work_resource_no">
		</div>
	</div>


	<!--  Tab -->
	<ul id="tabs" class="tabs_li_2tabs">
		<li><a href="#" tabid="tab1">基础信息</a></li>
		<li><a href="#" tabid="tab2">自定义数据</a></li>
	</ul>
	<div id="content">
		<div id="tab1">
			<div class="inputForm_content">
				<div class="formField_content">
					<label>描述:</label> <input type="text" name="workResourceFormMap.resource_desc" value="${workResourceFormMap.resource_desc}"
						class="formText_content" id="tbx_work_resource_des" />
				</div>
				<div class="formField_content">
				    
					<label>资源类型:</label> 
						<div class="hidden">
					         <input type="hidden" id="work_resource_type_id" value="${workResourceFormMap.resource_type_id}">
				        </div>
						<select class="formText_content"  id="select_work_resource_type_id"
						        name="workResourceFormMap.resource_type_id" value="${workResourceFormMap.resource_type_id}">
	                    </select>
				</div>
				<div class="formField_content">
					<label>状态:</label> 
					<select class="formText_content"  id="select_work_resource_status"  
					         name="workResourceFormMap.status" value="${workResourceFormMap.status}">
					<c:if test="${workResourceFormMap==null}">
                    <option value="1" selected="selected">已启用</option>
                    <option value="-1">已禁用</option>
                    </c:if>
                    <c:if test="${workResourceFormMap.status==1}">
                    <option value="1" selected="selected">已启用</option>
                    <option value="-1">已禁用</option>
                    </c:if>
                    <c:if test="${workResourceFormMap.status==-1}">
                    <option value="1">已启用</option>
                    <option value="-1" selected="selected">已禁用</option>
                    </c:if>
                    </select>
				</div>

				<div class="formField_content">
					<label>所属工作中心:</label> 
					<input type="text"
						dataValue="workcenter_no"
						relationId="input_workline_id"
						viewTitle="产线"
						data-url="/popup/queryWorkLine.shtml" 
						class="formText_query" id="tbx_workline_no" value="${workCenterFormMap.workcenter_no }"/>
					<input type="hidden" id="input_workline_id" validate_errormsg="产线不能为空！" dataValue="id" 
						name="workResourceFormMap.workline_id" value="${workResourceFormMap.workline_id }">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_workline_no">
				</div>
				<div class="formField_content">
					<label>创建人:</label> <span class="formText_content">${workResourceFormMap.create_user}</span>
				</div>
				<div class="formField_content">
					<label>创建时间:</label> <span class="formText_content">${workResourceFormMap.create_time}</span>
				</div>	
			</div>
		</div>
		<div id="tab2">
				<table class="mtable" id="work_resource_table">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
				</table>
		</div>
	</div>

	<!--  Tab -->
	</form>
</body>
</html>