<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<c:set var="ctx" value="${pageContext.request.contextPath}" />  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>工艺路线</title>
	<link rel="stylesheet" href="css/jquery_ui/jquery-ui.min.css">
	<script src="js/jquery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/jpopup.js"></script>
	<script src="js/jquery_ui/jquery-ui.min.js"></script>
	
	<script type="text/javascript" src="${ctx}/js/common/jpopup.js"></script>
	<script type="text/javascript" src="${ctx}/js/common/jpopup_loading.js"></script>
	<script src="js/workcenter_model/process_workflow/process_workflow_draw.js"></script>
	<script src="js/workcenter_model/process_workflow/process_workflow.js"></script>
	
	<script type="text/javascript" src="js/common/form_validate.js"></script>
	
	<link rel="stylesheet" href="css/workcenter_model/process_workflow/process_workflow.css">
</head> 
<body>
	<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
	</div>
	
	<div class="hidden">
		<input type="hidden" value="${page_res_id}" id="page_res_id">
	</div>
	
	<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		 <label class="mds_message_label"></label>
		 <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	</div>
	<div class="flow_body_div">
		<div class="flow_top">
			工艺路线绘制
		</div>
		<div class="flow_canvas_div">
			<div class="flow_left">
				<ul class="operation_menu">
					<li>
						<a class="one" href="#"><img src="icon/tree2.ico"> 正常操作</a>
						<ul>
							<c:forEach var="operation" items="${common}">
								<li class="node" data-value="${operation.id }"><a class="two" href="#"><img src="icon/tree2.png"> ${operation.operation_no}<font/></a></li>
							</c:forEach>
						</ul>
					</li>
					<li>
						<a class="one" href="#"><img src="icon/tree2.ico"> 维修操作</a>
						<ul>
							<c:forEach var="operation" items="${repair}">
								<li class="node rp" data-value="${operation.id }"><a class="two" href="#"><img src="icon/tree1.png"> ${operation.operation_no}</a></li>
							</c:forEach>
						</ul>            
					</li>
					<li>
						<a class="one" href="#"><img src="icon/tree2.ico"> 测试操作</a>
						<ul>
							<c:forEach var="operation" items="${test}">
								<li class="node rp" data-value="${operation.id }"><a class="two" href="#"><img src="icon/tree1.png"> ${operation.operation_no}</a></li>
							</c:forEach>
						</ul>            
					</li>
					<li>
						<a class="one" href="#"><img src="icon/tree2.ico"> 其它操作</a>
						<ul>
							<li class="node ot" data-value="1"><a class="three" href="#"><img class="filleted_corner" src="icon/tree3.png"> 开始</a></li>
							<li class="node ot" data-value="-1"><a class="three" href="#"><img class="filleted_corner" src="icon/tree3.png"> 结束</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="flow_canvas">
				<div class="draw_canvas" id="draw_canvas">
					
				</div>
			</div>
		</div>
		
		<div class="flow_properties">
			<form class="flow_form">
				<div class="hidden">
					<input type="hidden" name="processWorkFlowFormMap.id" value="${process_workflow.id}" id="process_workflow_id">
					<input type="hidden" value="${process_workflow.data}" id="process_workflow_data">
					<input type="hidden" value="${errorMessage}" id="errorMessage">
					<input type="hidden" value="${successMessage}" id="successMessage">
				</div>
				<div class="form_line content_search_button">
					<label class="form_label must flow_label">工艺路线：</label>
					<input type="text" class="form_input flow_input" name="processWorkFlowFormMap.process_workflow" id="process_workflow"
						dataValue="process_workflow"
						relationId= "process_workflow_version" 
						viewTitle="工艺路线"
						data-url="/popup/queryAllProcessWorkflow.shtml" 
						validate_allowedempty="N" validate_errormsg="工艺路线不能为空！" value="${process_workflow.process_workflow }">
					<input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="process_workflow">
				</div>
				<div class="form_line">
					<label class="form_label must flow_label">版本：</label>
					<input type="text" class="form_input flow_input2 majuscule" 
					dataValue="process_workflow_version"
					name="processWorkFlowFormMap.process_workflow_version" id="process_workflow_version"
					value="${process_workflow.process_workflow_version }"
					validate_allowedempty="N" validate_errormsg="工艺路线版本号必须输入一位字符！" maxlength="1" >
				</div>
				<div class="form_line">
					<label class="form_label must flow_label">描述：</label>
					<input type="text" class="form_input flow_input2" name="processWorkFlowFormMap.process_workflow_desc" id="process_workflow_desc"
					value="${process_workflow.process_workflow_desc }"
					validate_allowedempty="N" validate_errormsg="工艺路线描述不能为空！" >
				</div>
				<div class="form_line">
					<label class="form_label must flow_label">类型：</label>
					<select class="form_input flow_input" name="processWorkFlowFormMap.process_workflow_type" style="width: 142px;">
						<c:if test="${ process_workflow.process_workflow_type=='rework'}">
							<option value="normal">正常</option>
							<option value="rework" selected="selected">返工</option>
						</c:if>
						<c:if test="${ process_workflow.process_workflow_type!='rework'}">
							<option value="normal" selected="selected">正常</option>
							<option value="rework">返工</option>
						</c:if>
					</select>
				</div>
				<div class="form_line">
					<label class="form_label must flow_label">状态：</label>
					<select class="form_input flow_input" name="processWorkFlowFormMap.status" style="width: 142px;">
						<c:if test="${ process_workflow.status==-1}">
							<option value="1">可用</option>
							<option value="-1" selected="selected">不可用</option>
						</c:if>
						<c:if test="${ process_workflow.status!=-1}">
							<option value="1" selected="selected">可用</option>
							<option value="-1">不可用</option>
						</c:if>
					</select>
				</div>
				<div class="form_line">
					<label class="form_label must flow_label">允许跳线：</label>
					<select class="form_input flow_input" name="processWorkFlowFormMap.allowpass"  style="width: 142px;">
						<c:if test="${process_workflow.allowpass==-1 }">
							<option value="1">允许</option>
							<option value="-1" selected="selected">不允许</option>
						</c:if>
						<c:if test="${process_workflow.allowpass!=-1 }">
							<option value="1" selected="selected">允许</option>
							<option value="-1">不允许</option>
						</c:if>
					</select>
				</div>
				<!-- <div class="form_line_button_div">
					<input class="form_line_button" type="submit" value="保存">
					<input class="form_line_button" type="reset" value="重置">
				</div> -->
			</form>
		</div>
	</div>
</body>
</html>