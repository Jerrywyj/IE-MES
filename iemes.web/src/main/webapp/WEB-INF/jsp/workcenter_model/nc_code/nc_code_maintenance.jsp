<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>不良代码维护</title>

<script type="text/javascript" src="js/common/form_validate.js"></script>
<script type="text/javascript" src="js/common/jtab.js"></script>
<script type="text/javascript" src="js/common/mtable.js"></script>

<script type="text/javascript" src="js/workcenter_model/nc_code/nc_code_maintenance.js"></script>

</head>
<body>
	<form class="mds_tab_form">
		<div id="commanButtonsDiv" class=iemes_style_commanButtonsDiv>
			<c:forEach var="resRow" items="${res}">
				<c:if test="${resRow.type==2 && resRow.is_hide!=1}">
					<c:choose>
						<c:when test="${resRow.res_url=='btnSave'}">
							<input type="submit" id="btnSave" value="${resRow.res_name}">
						</c:when>
						<c:otherwise>
							<input type="button" id="${resRow.res_url}"
								value="${resRow.res_name}">
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</div>
		
		<div class="hidden">
			<input type="hidden" value="${page_res_id}" id="page_res_id">
			<input type="hidden" name="ncCodeFormMap.id" value="${ncCodeFormMap.id}" id="nc_code_id">
			<input type="hidden" value="${errorMessage}" id="errorMessage">
			<input type="hidden" value="${successMessage}" id="successMessage">
	    </div>

		<div id="pageNoticeInformation" class=iemes_style_NoticeDiv>
		   <label class="mds_message_label"></label>
		   <span class="iemes_style_NoticeButton"> <input type="button" submit="N" onclick="closeNoticeMessage();" value="关闭"> </span>
	    </div>

		<div class="inputForm_query">
			<div class="formField_query">
				<label class="must">站点:</label> <span class="formText_query">${site}</span>
			</div>
			<div class="formField_query">
				<label class="must">不良代码:</label>
				<input type="text"
			    dataValue="nc_code"
				viewTitle="不良代码"
				data-url="/popup/queryAllNcCode.shtml" 
				class="formText_query majuscule" id="tbx_nc_code" name="ncCodeFormMap.nc_code" value="${ncCodeFormMap.nc_code}"
				validate_allowedempty="N" validate_errormsg="请输入不良代码！" />
			    <input type="button" submit="N" value="检索" onclick="operationBrowse(this)" textFieldId="tbx_nc_code">	
			</div>
		</div>


		<!--  Tab -->
		<ul id="tabs" class="tabs_li_2tabs">
			<li><a href="#" tabid="tab1">基础信息</a></li>
			<li><a href="#" tabid="tab2">自定义数据</a></li>
		</ul>
		<div id="content">
			<div id="tab1">
				<div class="inputForm_content">
					<div class="formField_content">
						<label>描述:</label> <input type="text" class="formText_content"
						    name="ncCodeFormMap.nc_code_desc" value="${ncCodeFormMap.nc_code_desc}"
							id="tbx_nc_code_desc" />
					</div>
					<div class="formField_content">
						<label>不良代码组:</label>
						<input type="text" class="formText_content"
						dataValue="nc_code_group_no" 
						viewTitle="不良代码组"
						relationId="tbx_nc_code_group_id"
						data-url="/popup/queryAllNcCodeGroup.shtml" 
						id="tbx_nc_code_group_no" value="${nc_code_group_no}"/>
					    <input type="hidden" id="tbx_nc_code_group_id" dataValue="id" name="ncCodeFormMap.nc_code_group_id" value="${nc_code_group_id}">
					    <input type="button" submit="N" value="检索"  onclick="operationBrowse(this)"  textFieldId="tbx_nc_code_group_no">
					</div>
					<div class="formField_content">
						<label>状态:</label> 
						<select class="formText_content"  id="select_nc_code_status"  name="ncCodeFormMap.status" value="${ncCodeFormMap.status}">
						<c:if test="${ncCodeFormMap==null || ncCodeFormMap.status==1}">
	                    <option value=1 selected="selected">可用</option>
	                    <option value=-1>不可用</option>
	                    </c:if>
	                    <c:if test="${ncCodeFormMap.status==-1}">
	                    <option value=1 >可用</option>
	                    <option value=-1 selected="selected">不可用</option>
	                    </c:if>
	                    </select>
					</div>
					<div class="formField_content">
						<label>创建人:</label> <span class="formText_content">${ncCodeFormMap.create_user}</span>
					</div>
					<div class="formField_content">
						<label>创建时间:</label> <span class="formText_content">${ncCodeFormMap.create_time}</span>
					</div>
				</div>
			</div>
			<div id="tab2">
				<table class="mtable" id="uDData">
					<tr>
						<th data-value="key">数据字段</th>
						<th data-value="value">值</th>
					</tr>
					<c:forEach var="key" items="${dataKeys}">
						<tr>
							<td data-value="${key.id}">${key.data_label}</td>
							<td data-value="${key.value}">
							   <input type='text' value="${key.value}">
							</td>
						</tr>
					</c:forEach>
			    </table>
			</div>
		</div>

		<!--  Tab -->
	</form>
</body>
</html>