$(document).ready(function() {
	
	//时间控件
	pickDatetimeControl($("#input_begin_time"));
	pickDatetimeControl($("#input_end_time"));
	
	//查询
	$("form").submit(function() {
		var data = $("form").serialize();
		
		$.post(rootPath + "/report/getReportData.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("查询成功");
				debugger;
				$.MTable.delAll("production_record_table");
				for (var i=0;i<data.result.length;i++) {
					var rowData = data.result[i];
					var row = "<tr>" +
						"<td class='mtable_head_hide'>"+rowData.shoporder_id+"</td>" +
						"<td>"+rowData.shoporder_no+"</td>" +
						"<td>"+rowData.sfc+"</td>" +
						"<td>"+rowData.sfc_status+"</td>" +
						"<td>"+rowData.item_no+"</td>" +
						"<td>"+rowData.process_workflow+"</td>" +
						"<td>"+rowData.shoporder_status+"</td>" +
						"<td>"+rowData.workshop_no+"</td>" +
						"<td>"+rowData.workline_no+"</td>" +
						"<td>"+new Date(rowData.create_time.time).toLocaleString()+"</td>" +
						"<td>"+rowData.create_user+"</td>" + 
						"<td><a href='#' class='sfc_more_info' id="+rowData.sfc+">详细信息</a></td>" +
						"</tr>";
					$.MTable.addRow("production_record_table",row);
				}
				$("a.sfc_more_info").bind("click",showMoreInfo);
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	/**
	 * 详细信息
	 */
	var showMoreInfo = function (){
		var rowTds = $(this).parent().parent().find("td");
		var rowData = {};
		$(rowTds).each(function (index,element) {
			var head = $('#production_record_table th:eq('+index+')').data("value");
			rowData[head] = $(element).text();
		});
		var shoporder_id = rowData.shoporder_id;
		var sfc = this.id;
		debugger;
		
		var data_url = rootPath + "/report/production_record_flowstep_report.shtml?formMap.sfc="+sfc+"&formMap.shoporder_id="+shoporder_id;			
		window.open(data_url, "_blank");
	};
})

