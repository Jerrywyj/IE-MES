$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {
		debugger;
		var rows = $('table.mtable tr');
		var tableData = [];
		for (var j=2; j<rows.length; j++) {
			var row = rows[j].children;
			var headers = rows[0].children;
			
			headerKeys = headers[1].attributes.value.value;
			var check_status = row[0].children[0].checked;
			if (check_status) {
				tableData.push(row[1].attributes.value.value);
			}
		}
		
		
		var data = $("form").serialize();
		var users = $('#selectGroups')[0].children;
		var userSelectGroups = "";
		for (var i=0;i<users.length;i++) {
			userSelectGroups += users[i].value;
			if (i!=users.length-1) userSelectGroups += ","
		}
		data = data + "&roleFormMap.userSelectGroups="+userSelectGroups+"&roleFormMap.tableData="+tableData;
		$.post(rootPath + "/system/role/saveRole.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("角色信息保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var role_key = $('#tbx_role_id').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/role/queryRole.shtml?role_key="+role_key+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/role/list.shtml?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var role_id = $('#role_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/system/role/delRole.shtml?role_id="+role_id+"&page_res_id="+page_res_id);
		});
	})
	
	//选择框点击事件
	$('input:checkbox').click(function () {
		var thisClassName = this.parentElement.parentElement.children[1].className;
		var thisClassLevel = thisClassName.replace('role_mtable_col2 mtable_role_level_','');
		var startIndex = this.parentElement.parentElement.rowIndex;
		var rs = this.checked;
		$('#role_resource_table tr').each(function (index, element){
			if (index>startIndex) {
				var className = $(this).children()[1].className;
				var classLevel = className.replace('role_mtable_col2 mtable_role_level_','');
				if (thisClassLevel>=classLevel) {
					return false;
				}
				if (rs) {
					$(this).find("input:checkbox").prop("checked", true);
				}else {
					$(this).find("input:checkbox").prop("checked", false);
				}
			}
		})
	})

})