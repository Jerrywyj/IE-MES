$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		$.post(rootPath + "/system/menu/saveMenu.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("菜单信息保存成功");
			}else {
				//showErrorNoticeMessage("菜单信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var menu_key = $('#tbx_menu_key').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/menu/queryMenuData.shtml?menu_key="+menu_key);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/system/menu/menu_manager.shtml");
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var menu_id = $('#menu_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/system/menu/delMenu.shtml?menu_id="+menu_id);
		});
	})
	
	//绑定上层菜单列表
	var url = rootPath + '/system/menu/menu_list.shtml';
	var dataPMenu = "";
	var data = "";
	$.ajax({
        url:url,
        dataType: 'json',
        async: false,//默认异步调用 false：同步
        data:data,
        type:'POST',
        success:function(data){
        	dataPMenu = data;
        }
    });
	if (dataPMenu != null) {
		var currPMenu = $("#menu_parent_id").val();
		var h = "<option value='0'>------顶级目录------</option>";
		for ( var i = 0; i < dataPMenu.length; i++) {
			if($.trim(currPMenu) == $.trim(dataPMenu[i].id)){
				h+="<option selected=\"selected\" value='" + dataPMenu[i].id + "'>"+ dataPMenu[i].res_name + "</option>";
			}else{
				h+="<option value='" + dataPMenu[i].id + "'>"+ dataPMenu[i].res_name + "</option>";
			}			
		}
		$("#select_menu_parent_id").html(h);
	} else {
		showErrorNoticeMessage("获取菜单信息错误，请联系管理员！");
	}
})