$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	//保存
	$("form").submit(function() {
		var txt = ue.getContent();
		var data = $("form").serializeArray();
		var d = {};
		d.name = "sopFormMap.sop_content";
		d.value = txt;
		data.push(d);
		$.post(rootPath + "/work_scheduling/sop_manager/saveSop.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("SOP信息保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var resources_id = $('#select_resources_id').val();
		var resources_text = $('#select_resources_id').find("option:selected").text();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_scheduling/sop_manager/querySop.shtml?resources_id="+resources_id +"&resources_text="+resources_text);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_scheduling/sop_manager/sop_manager.shtml");
	})
	
	//绑定菜单列表
	var url = rootPath + '/system/menu/menu_list.shtml';
	var dataPMenu = "";
	var data = "";
	$.ajax({
        url:url,
        dataType: 'json',
        async: false,//默认异步调用 false：同步
        data:data,
        type:'POST',
        success:function(data){
        	dataPMenu = data;
        }
    });
	if (dataPMenu != null) {
		var currPMenu = $("#resources_id").val();
		var h = "";
		for ( var i = 0; i < dataPMenu.length; i++) {
			//去掉菜单和按钮级别的
			if($.trim(dataPMenu[i].type) == "0" || $.trim(dataPMenu[i].type) == "2"){
				continue;
			}
			if($.trim(currPMenu) == $.trim(dataPMenu[i].id)){
				h+="<option selected=\"selected\" value='" + dataPMenu[i].id + "'>"+ $.trim(dataPMenu[i].res_name) + "</option>";
			}else{
				h+="<option value='" + dataPMenu[i].id + "'>"+ $.trim(dataPMenu[i].res_name) + "</option>";
			}			
		}
		$("#select_resources_id").html(h);
	} else {
		showErrorNoticeMessage("获取菜单信息错误，请联系管理员！");
	}
	
})

