function pickDatetimeControl(datatimepickerInput) {
	// datetime_type : date:只显示日期，time:只显示点时间，精确到秒，datetime:显示全部时间,默认显示全部时间
	var datetimeFormatType = $(datatimepickerInput)
			.attr("datetime_format_type");
	if (datetimeFormatType == null || $.trim(datetimeFormatType) == "") {
		return;
	} else {
		if (datetimeFormatType == "date") {
			$(datatimepickerInput).datetimepicker({
				lang : 'ch',
				timepicker : false,
				format : 'Y-m-d',
				formatDate : 'Y/m/d'
			});
		} else if (datetimeFormatType == "time") {
			$(datatimepickerInput).datetimepicker({
				lang : 'ch',
				datepicker : false,
				format : 'H:i:00',
				step : 1
			});
		} else if (datetimeFormatType == "datetime") {
			$(datatimepickerInput).datetimepicker({
				lang : 'ch',
				format : 'Y-m-d H:i:00',
			});
		} else {
			$(datatimepickerInput).datetimepicker({
				lang : 'ch',
				format : 'Y-m-d H:i:s',
			});
		}
	}
}