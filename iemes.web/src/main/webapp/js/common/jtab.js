$(document).ready(function() {
	$("#tabs li:first").attr("id", "current");
	$('#content').children(':not(:first)').hide();
	
	$('#tabs a').click(function(e) {
		$("#tabs li").not(this).attr("id", "");
		$('#content').children().hide();
		$(this).parent().attr("id", "current");
		$('#' + $(this).attr('tabid')).show();
	})
});
