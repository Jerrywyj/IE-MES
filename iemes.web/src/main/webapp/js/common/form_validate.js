//说明:
//属性:validate_allowedempty  输入文本是否可以为空,“Y”:允许;“N”:不允许,默认为"Y"
//属性:validate_datatype  输入文本的类型，包括:number_int:整数;number_decimal:小数;date:日期;time:时间;
//属性:validate_regex  输入文本的正则表达式
//属性:validate_errormsg  输入文本不符合规范时的显示信息，缺省值为“该属性的输入值不符合规范,请重新输入!”		
//属性:validate_min_len  输入文本不符合规范时的显示信息，缺省值为“长度不能大于XXX!”		

$(document).ready(function() {
	var page_errormsg = "";
	var isSubmitClick = false;
	var validate_min_len = "";
	
	$("form").on("blur","input",function() {
		if(!isSubmitClick){	
			return;
		}	
		var validate_errormsg = "";
		var $noticeMsgDiv = $("#pageNoticeInformation");
		var $noticeMsgLabel = $("#pageNoticeInformation").find("label");
				
		var validate_allowedempty = true;
		validate_allowedempty = $.trim($(this).attr("validate_allowedempty"));
		if (validate_allowedempty == "N") {
			validate_allowedempty = false;
		}else{
			validate_allowedempty = true;
		}		
		validate_errormsg = $.trim($(this).attr("validate_errormsg"));
		validate_min_len = $.trim($(this).attr("validate_min_len"));
		validate_max_len = $.trim($(this).attr("validate_max_len"));
		if (validate_errormsg == "") {
			validate_errormsg = "输入值不符合规范,请重新输入!";
		}
		var validate_datatype = "";
		validate_datatype = $.trim($(this).attr("validate_datatype"));
		var validate_regex = "";
		validate_regex = $.trim($(this).attr("validate_regex"));

		var inputData = $.trim(this.value);
		if (validate_max_len!=undefined && validate_max_len!=null && validate_max_len!="") {
			var regex = /^-?\d+$/;
			if (!regex.test(validate_max_len)) {
				page_errormsg = "validate_max_len的值必须为数字！";
				showErrorNoticeMessage(page_errormsg);
				return;
			}
			if (inputData.length>validate_max_len) {
				page_errormsg = validate_errormsg+"输入的值的长度不能超过："+validate_min_len;
				
				showErrorNoticeMessage(page_errormsg);
				return;
			}
		}
		
		if (validate_min_len!=undefined && validate_min_len!=null && validate_min_len!="") {
			var regex = /^-?\d+$/;
			if (!regex.test(validate_min_len)) {
				page_errormsg = "validate_min_len的值必须为数字！";
				showErrorNoticeMessage(page_errormsg);
				return;
			}
			if (inputData.length<validate_min_len) {
				page_errormsg = validate_errormsg+"输入的值的长度不能小于："+validate_max_len;
				showErrorNoticeMessage(page_errormsg);
				return;
			}
		}
		
		// 1.判断输入文本不能为空
		if (!validate_allowedempty) {
			if (inputData == "" || inputData.length < 0) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
				return;
			}
		}
		
		// 2.判读输入文本为数字
		//验证数字-整数
		if (validate_datatype == "number_int") {
			var regex = /^-?\d+$/;
			if (!regex.test(inputData)) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
			}
		}
		//验证数字-小数
		else if (validate_datatype == "number_decimal") {
			var regex = /^\d+(\.\d+)?$/;
			if (!regex.test(inputData)) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
			}
		}

		// 3.判断输入文本为日期/时间格式
		//日期2019-09-01
		else if (validate_datatype == "date") {
			var regex = /^[0-9]{4}-[0-1]?[0-9]{1}-[0-3]?[0-9]{1}$/;
			if (!regex.test(inputData)) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
			}
		}

		//时间01:02:13
		else if (validate_datatype == "time") {
			var regex = /^(0\d{1}|1\d{1}|2[0-3]):[0-5]\d{1}:([0-5]\d{1})$/;
			if (!regex.test(inputData)) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
			}
		}

		//4.正则表达判断
		if (validate_regex != "" && validate_regex.length > 0) {
			var regex = validate_regex;
			if (!regex.test(inputData)) {
				page_errormsg = validate_errormsg;
				showErrorNoticeMessage(validate_errormsg);
			}
		}
	});

	$(":submit").click(function() {
		var isSubmit = false;
		isSubmit = $.trim($(this).attr("submit"));
		if (isSubmit == "N") {
			isSubmit = false;
		} else {
			isSubmit = true;
		}
		
		if (isSubmit) {
			isSubmitClick = true;
			page_errormsg = "";
			$("form :input").trigger("blur");
			//var numError = $("#pageNoticeInformation").find("label").length;
			isSubmitClick = false;
			var numError = page_errormsg.length;
			if (numError > 0) {
				return false;
			}else{
				$("#pageNoticeInformation").fadeOut("slow");
				$.MsgLoading.Loading("系统提示", "资料处理中，请耐心等候。。。。。。");  
				//TODO 显示loading
			}
		}
	});
});

