$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var udata = $.MTable.getTableData("operation_table");
		data = data + "&operationFormMap.operationUdata="+JSON.stringify(udata);
		
		$.post(rootPath + "/workcenter_model/operation/saveOperation.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("操作保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var operation_no = $('#tbx_operation_no').val();
		var version = $('#tbx_opreation_version').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/operation/queryOperation.shtml?operation_no="+operation_no+"&version="+version+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/operation/operation_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var operation_id = $('#operation_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/operation/delOperation.shtml?operation_id="+operation_id+"&page_res_id="+page_res_id);
		});
	})
})