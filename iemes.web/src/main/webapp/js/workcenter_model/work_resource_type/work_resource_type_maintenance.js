$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();
		var workResourceSelectGroups = $('#selectGroups')[0].children;
		var selectedWorkResources = "";	
		for (var i=0;i<workResourceSelectGroups.length;i++) {
			selectedWorkResources += workResourceSelectGroups[i].value;
			if (i!=workResourceSelectGroups.length-1) selectedWorkResources += ",";
		}
		var workResourceUnSelectGroups = $('#groupsForSelect')[0].children;
		var unSelectedWorkResources = "";
		for (var i=0;i<workResourceUnSelectGroups.length;i++) {
			unSelectedWorkResources += workResourceUnSelectGroups[i].value;
			if (i!=workResourceUnSelectGroups.length-1) unSelectedWorkResources += ",";
		}
		
		data = data + "&workResourceTypeFormMap.selectedWorkResources="+selectedWorkResources;
		data = data + "&workResourceTypeFormMap.unSelectedWorkResources="+unSelectedWorkResources;
		
		$.post(rootPath + "/workcenter_model/work_resource_type/saveWorkResourceType.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("资源类型信息保存成功");
			}else {
				//showErrorNoticeMessage("资源类型信息保存失败："+data.message);
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var work_resource_type = $('#tbx_work_resource_type').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/work_resource_type/queryWorkResourceType.shtml?work_resource_type="+work_resource_type+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/work_resource_type/work_resource_type_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var work_resource_type_id = $('#work_resource_type_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/work_resource_type/delWorkResourceType.shtml?work_resource_type_id="+work_resource_type_id+"&page_res_id="+page_res_id);
		});
	})
})