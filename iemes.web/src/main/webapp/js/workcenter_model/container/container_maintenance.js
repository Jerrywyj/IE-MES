$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	
	var page_res_id = $('#page_res_id').val();
	
	//保存
	$("form").submit(function() {
		var data = $("form").serialize();
		var udata = $.MTable.getTableData("container_type_table");
		data = data + "&containerFormMap.containerUdata="+JSON.stringify(udata);
		
		$.post(rootPath + "/workcenter_model/container/saveContainer.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("容器保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var container_type_no = $('#tbx_container_type_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/container/queryContainer.shtml?container_type_no="+container_type_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/container/container_manager.shtml?page_res_id="+page_res_id);
	})
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var container_type_id = $('#container_type_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/container/delContainer.shtml?container_type_id="+container_type_id+"&page_res_id="+page_res_id);
		});
	})
	
})