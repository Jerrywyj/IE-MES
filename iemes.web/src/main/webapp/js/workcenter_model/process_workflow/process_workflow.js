$(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//处理数据加载
	if ($('#process_workflow_data').val()!=""){
		$.Mflow.loadData($('#process_workflow_data').val());
	}
	
	//保存
	$('#btnSave').click(function (){
		$(".flow_form").submit();
	})
	
	$(".flow_form").submit(function() {
		var process_workflow = $('#process_workflow').val();
		var process_workflow_desc = $('#process_workflow_desc').val();
		if (process_workflow==null || 
			process_workflow=="") {
			showErrorNoticeMessage("工艺路线不能为空");
			return false;
		}
		if (process_workflow_desc==null ||
			process_workflow_desc=="") {
			showErrorNoticeMessage("工艺路线描述不能为空");
			return false;
		}
		
		var data = $("form").serialize();
		var nexus = $.Mflow.getData().nexus;
		if ($.Mflow.judgeFlow()) {
			var flowData = $.Mflow.getFlowData();
			var mdata = $.Mflow.getData();
			data = data + "&processWorkFlowFormMap.flowData="+JSON.stringify(flowData);
			data = data + "&processWorkFlowFormMap.mdata="+JSON.stringify(mdata);
			
			$.post(rootPath + "/workcenter_model/process_workflow/saveProcessWrokFlow.shtml", data, function(e, status, xhr){
				var data= JSON.parse(e);
				if (data.status) {
					showSuccessNoticeMessage("工艺路线保存成功");
				}else {
					showErrorNoticeMessage(data.message);
				}
			},"json")
		}
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var processWorkflow = $('#process_workflow').val();
		var version = $('#process_workflow_version').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/process_workflow/queryProcessWorkFlow.shtml?process_workflow="+processWorkflow+"&version="+version+"&page_res_id="+page_res_id);
	});
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/workcenter_model/process_workflow/process_workflow.shtml?page_res_id="+page_res_id);
	});
	
	//删除
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("系统提示","该条数据将会被删除，确定继续吗？",function (){
			var process_workflow_id = $('#process_workflow_id').val();
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/workcenter_model/process_workflow/delProcessWorkFlow.shtml?process_workflow_id="+process_workflow_id+"&page_res_id="+page_res_id);
		});
	});
})