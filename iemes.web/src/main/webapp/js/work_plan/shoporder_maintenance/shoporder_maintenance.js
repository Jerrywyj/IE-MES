$(document).ready(function() {
	
	if (!stringIsNull($('#errorMessage').val())) {
		showErrorNoticeMessage($('#errorMessage').val());
	}
	
	if (!stringIsNull($('#successMessage').val())) {
		showSuccessNoticeMessage($('#successMessage').val());
	}
	var page_res_id = $('#page_res_id').val();
	//时间控件
	pickDatetimeControl($("#tbx_shop_order_plan_start_time"));
	pickDatetimeControl($("#tbx_shop_order_plan_end_time"));
	
	//保存
	$("form").submit(function() {	
		var data = $("form").serialize();		
		var udefine_data = $.MTable.getTableData("uDData");
		/*udefine_data = JSON.stringify(udefine_data).replace(/"/g,"'");*/
		udefine_data = JSON.stringify(udefine_data);
		
		data = data+"&shopOrderFormMap.udefined_data="+udefine_data;
		
		$.post(rootPath + "/work_plan/shoporder_maintenance/saveShoporder.shtml", data, function(e, status, xhr){
			var data= JSON.parse(e);
			if (data.status) {
				showSuccessNoticeMessage("工单信息保存成功");
			}else {
				showErrorNoticeMessage(data.message);
			}
		},"json")
		return false;
	});
	
	//检索
	$('#btnQuery').click(function (){
		var shoporder_no = $('#tbx_shoporder_no').val();
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/shoporder_maintenance/queryShoporder.shtml?shoporder_no="+shoporder_no+"&page_res_id="+page_res_id);
	})
	
	//清除
	$('#btnClear').click(function (){
		var tb = $(".index_centent");
		tb.empty();
		tb.load(rootPath + "/work_plan/shoporder_maintenance/shoporder_maintenance.shtml?page_res_id="+page_res_id);
	})
	
	$('#btnDelete').click(function (){
		$.MsgBox.Confirm("","该条数据将会被删除，确定继续吗？",function (){
			var shoporder_id = $('#shoporder_id').val();
			
			var tb = $(".index_centent");
			tb.empty();
			tb.load(rootPath + "/work_plan/shoporder_maintenance/delShoporder.shtml?shoporder_id="+shoporder_id+"&page_res_id="+page_res_id);
		});
	})
	
})

